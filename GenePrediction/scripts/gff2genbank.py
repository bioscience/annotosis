from snakemake.shell import shell
#from base import Base
from pyfaidx import Fasta
import os, sys, re

#################################################
def extractGeneRegion(fasta, scafId, start, end, flank, revComp):
    '''
    '''
    seq = None
    adjusted = False
    try:
        s = start - flank
        e = end + flank
        if s <= 0: s, adjusted = 1, True
        if e > len(fasta[scafId]): e, adjusted = len(fasta[scafId]), True
        if revComp == True:
            seq = fasta[scafId][s - 1:e].reverse.complement.seq
        else:
            seq = fasta[scafId][s - 1:e].seq
    except KeyError:
        pass
    return seq, start - flank - 1, end + flank - e, adjusted
        
#################################################
def adjustIndex(geneGff, flank, ds, de, scafLen, scafId, revComp):
    '''
    '''
    lines = []
    p1 = re.compile("exon\d+")
    p2 = re.compile("cds\d+")
    items = geneGff[0].strip().split('\t')
    veryS, veryE = int(items[3]), int(items[4])
    if revComp == True:
        veryS, veryE = scafLen - veryE + 1, scafLen - veryS + 1
    for line in geneGff:
        items = line.strip().split('\t')
        if items[2] == "mRNA": continue
        items[0] = scafId
        if items[2] == "exon":
            items[-1] = p1.sub("exon", items[-1], count=1)
            items[2] = "mRNA"
        if items[2] == "CDS":
            items[-1] = p2.sub("cds", items[-1], count=1)
        if revComp == True:
            start, end = int(items[3]), int(items[4])
            items[3], items[4] = "%d" %(scafLen - end + 1), "%d" %(scafLen - start + 1)
            items[6] = "+"
            ds, de = -de, -ds
        fs, fe = flank, flank
        if ds < 0:
            fs += ds
        if de > 0:
            fe -= de
        start, end = int(items[3]), int(items[4])
        start, end = fs + start - veryS + 1, end + fe - veryS + 1
        items[3], items[4] = "%d" %start, "%d" %end
        lines.append('\t'.join(items))
    return lines    
    
# Placeholder for optional parameters
#minPtsLen = int(snakemake.params.get("minPtsLen", ""))
#minExonSize = int(snakemake.params.get("minExonSize", ""))
#minIntronSize = int(snakemake.params.get("minIntronSize", ""))
# Run log
#log = snakemake.log_fmt_shell()

scafsFile = snakemake.input.get("reference")
gffFile = snakemake.input.get("gff")

flank = 1001

shell("rm -f %s %s" %(snakemake.output[0], snakemake.output[1]))

#base = Base()
fasta = Fasta(scafsFile)
geneGff = None
scafId, start, end = None, None, None
cnt = 0
with open(gffFile) as handle:
    for line in handle:
        if line[0] == "#": continue
        items = line.strip().split('\t')
        if items[2] == "gene":
            if geneGff != None:
                revComp = False
                if geneGff[0].strip().split('\t')[6] == "-": revComp = True
                seq, deltaS, deltaE, adjusted = extractGeneRegion(fasta, scafId, start, end, flank, revComp)
                if adjusted == False: # Full flanking regions are required
                    with open(snakemake.output[2], 'w') as handle:
                        handle.write(">gene%d\n%s\n" %(cnt, seq))
                        #handle.write(">%s\n%s\n" %(scafId, seq))
                    with open(snakemake.output[3], 'w') as handle:
                        geneGff = adjustIndex(geneGff, flank, deltaS, deltaE, len(seq), "gene%d" %cnt, revComp)
                        handle.write("%s\n" %'\n'.join(geneGff))
                    #base.shell("gff2gbSmallDNA.pl gene.gff gene.fna 1000 gene.genbank") # 2> error.error")
                    #base.shell("sed -i '3s/^/     source          1..%d\\n/' gene.genbank" %len(seq))
                    #base.shell("cat gene.genbank >> training.genbank", doprint=False)
                    shell("gff2gbSmallDNA.pl %s %s 1000 %s && cat %s >> %s && cat %s >> %s" %(snakemake.output[3], snakemake.output[2], snakemake.output[4], snakemake.output[4], snakemake.output[0], snakemake.output[2], snakemake.output[1]))
                    #shell("gff2gbSmallDNA.pl %s/gene.gff %s/gene.fna 1000 %s/gene.genbank && cat %s/gene.genbank >> %s/training.genbank && cat %s/gene.fna >> %s/training.fna" %(wd, wd, wd, wd, wd, wd, wd))
            cnt += 1
            scafId, start, end = items[0], int(items[3]), int(items[4])
            geneGff = []
        geneGff.append(line)
                           
    if geneGff != None:
        revComp = False
        if geneGff[0].strip().split('\t')[6] == "-": revComp = True
        seq, deltaS, deltaE, adjusted = extractGeneRegion(fasta, scafId, start, end, flank, revComp)
        if adjusted == False:
            with open(snakemake.output[2], 'w') as handle:
                handle.write(">gene%d\n%s\n" %(cnt, seq))
            with open(snakemake.output[3], 'w') as handle:
                geneGff = adjustIndex(geneGff, flank, deltaS, deltaE, len(seq), "gene%d" %cnt, revComp)
                handle.write("%s\n" %'\n'.join(geneGff))
            #base.shell("seqret -sequence gene.fna -feature -fformat gff -fopenfile gene.gff -osformat genbank -osname_outseq gene -ofdirectory_outseq gbk_file -auto", doprint=False)
            #base.shell("gff2gbSmallDNA.pl gene.gff gene.fna 1000 gene.genbank")
            #base.shell("sed -i '3s/^/     source          1..%d\\n/' gene.genbank" %len(seq))
            #base.shell("cat gene.genbank >> training.genbank", doprint=False)
            shell("gff2gbSmallDNA.pl %s %s 1000 %s && cat %s >> %s && cat %s >> %s" %(snakemake.output[3], snakemake.output[2], snakemake.output[4], snakemake.output[4], snakemake.output[0], snakemake.output[2], snakemake.output[1]))
    #shell("rm -rf %s/gene.fna %s/gene.gff %s/gene.genbank" %(wd, wd, wd))    

#snakemake.output[0] = "%s/training.genbank" %wd
#snakemake.output[1] = "%s/training.fna" %wd

