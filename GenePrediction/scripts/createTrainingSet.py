from snakemake.shell import shell
import re

# Placeholder for optional parameters
minPtsLen = int(snakemake.params.get("minPtsLen", ""))
minExonSize = int(snakemake.params.get("minExonSize", ""))
minIntronSize = int(snakemake.params.get("minIntronSize", ""))
# Run log
#log = snakemake.log_fmt_shell()

ptsFa = snakemake.input.get("ptss")
cdsFa = snakemake.input.get("cdss")
gff = snakemake.input.get("gffInput")

# nr.cds.fasta
#>STRG.9989.2.p1 GENE.STRG.9989.2~~STRG.9989.2.p1  ORF type:complete len:156 (-),score=5.89 STRG.9989.2:1230-1697(-)
#>TRINITY_GG_1000_c0_g1_i1.p4.p1 GENE.TRINITY_GG_1000_c0_g1_i1.p4~~TRINITY_GG_1000_c0_g1_i1.p4.p1  ORF type:complete len:101 (+),score=1.57 TRINITY_GG_1000_c0_g1_i1.p4:1-303(+)
# gmap.pts.fasta
#>TRINITY_GG_1805_c0_g1_i7.p1.p1.mrna1_1 CDS=68-137
#>STRG.10014.1.p1.mrna1_1 CDS=1-525

from pyfaidx import Fasta
dInfo = {}
fasta = Fasta(cdsFa)
for hdr in fasta.keys():
    dInfo[hdr] = len(fasta[hdr])

trsIds = []
with open(ptsFa) as handle:
    for line in handle:
        if line[0] == ">":
            #print(line)
            try:
                trsId, cdsHit = line.strip().replace(">", "").split()
            except ValueError:
                print("### WARNING: Header format inconsistent %s ###" %line.strip(), file=sys.stderr)
                continue
            trsId = trsId.split(".mrna")[0]
            start, end = cdsHit.replace("CDS=", "").split('-')
            if int(end) - int(start) + 1 == dInfo[trsId]:
                trsIds.append(trsId)

fasta = Fasta(ptsFa)
dMap = {}
for trsId in trsIds:
    dMap[trsId] = []
    for key in fasta.keys():
        if trsId in key:
            dMap[trsId].append(key)

trainingIds = []
for trsId in trsIds:
    if len(dMap[trsId]) > 1: # Avoid overlapping genes
        print ("### WARNING: more than one hit for transcript %s" %trsId)
        #continue
    seq = fasta[dMap[trsId][0]][:].seq.upper()
    if seq[0] == "M" and seq[-1] == "*" and seq.count("*") == 1 and len(seq) >= minPtsLen:
        trainingIds.append(trsId)

dCdss = {}
dGenes = {}
dTrss = {}
with open(gff) as handle:
    for line in handle:
        if line[0] == "#": continue
        items = line.strip().split('\t')
        if items[2] == "gene": # To check overlaps
            trsId = items[-1].split(';')[0].replace("ID=","")
            trsId = '.'.join(trsId.split('.')[:-1])
            dGenes[trsId] = [items[0], int(items[3]), int(items[4])]
        if items[2] == "mRNA": # To filter out indel ones
            trsId = items[-1].split(';')[0].replace("ID=","")
            trsId = '.'.join(trsId.split('.')[:-1])
            #print (items[-1])
            #print (re.search("indels=([0-9]+);", items[-1]).group(1))
            indelCnt=int(re.search(r"indels=([0-9]+);", items[-1]).group(1))
            #print (indelCnt)
            mismatchCnt=int(re.search(r"mismatches=([0-9]+);", items[-1]).group(1))
            dTrss[trsId] = [indelCnt, mismatchCnt]
        if items[2] == "CDS":
            trsId = items[-1].split(';')[0].replace("ID=","")
            trsId = '.'.join(trsId.split('.')[:-2])
            items[-1] = 'transcript_id "%s"' %trsId 
            try:
                dCdss[trsId].append('\t'.join(items))
            except KeyError:
                dCdss[trsId] = []
                dCdss[trsId].append('\t'.join(items))

# Remove ones with short exons or introns
dTrainingCdss = {}
dTrainingGenes = {}
for trsId in trainingIds:
    failed = False
    for line in dCdss[trsId]:
        items = line.split('\t')
        if int(items[4]) - int(items[3]) < minExonSize:
            failed = True
            break
    if failed == False:
        introns = []
        prevItems = None
        for line in dCdss[trsId]:
            items = line.split('\t')
            if prevItems != None and items[6] == "-":
                if int(items[4]) - int(prevItems[3]) < minIntronSize:
                    failed == True
                    break
            elif prevItems != None and items[6] == "+":
                if int(items[3]) - int(prevItems[4]) < minIntronSize:
                    failed == True
            prevItems = items
    if failed == False:
        dTrainingCdss[trsId] = dCdss[trsId]
        dTrainingGenes[trsId] = dGenes[trsId]

# Remove overlapping transcripts
toRemove = set()
keys = sorted(dTrainingGenes.keys())
for i in range(len(keys)):
    for j in range(len(keys)):
        if j > i:
            trsId1, trsId2 = keys[i], keys[j]
            scaf1, scaf2 = dTrainingGenes[trsId1][0], dTrainingGenes[trsId2][0]
            if scaf1 == scaf2:
                start1, end1 = int(dTrainingGenes[trsId1][1]), int(dTrainingGenes[trsId1][2])
                start2, end2 = int(dTrainingGenes[trsId2][1]), int(dTrainingGenes[trsId2][2])
                if (end1 >= start2 and start1 <= end2) or (end2 >= start1 and start2 <= end1):
                    if end1 - start1 > end2 - start2: # Remove shorter overlapping one
                        toRemove.add(trsId2)
                    else:
                        toRemove.add(trsId1)

dTrsKeep = {}
keep = sorted(set(keys) - toRemove) # Remove overlaps
                        
#chr2_RagTag_pilon_pilon_pilon	GenRef	gene	227481	228593	.	-	.	ID=STRG.10035.5.p2.path1;Name=STRG.10035.5.p2;Dir=antisense
#chr2_RagTag_pilon_pilon_pilon	GenRef	mRNA	227481	228593	.	-	.	ID=STRG.10035.5.p2.mrna1;Name=STRG.10035.5.p2;Parent=STRG.10035.5.p2.path1;Dir=antisense;coverage=100.0;identity=100.0;matches=378;mismatches=0;indels=0;unknowns=0
#chr2_RagTag_pilon_pilon_pilon	GenRef	exon	228562	228593	100	-	.	ID=STRG.10035.5.p2.mrna1.exon1;Name=STRG.10035.5.p2;Parent=STRG.10035.5.p2.mrna1;Target=STRG.10035.5.p2 347 378 -
#chr2_RagTag_pilon_pilon_pilon	GenRef	exon	227481	227826	100	-	.	ID=STRG.10035.5.p2.mrna1.exon2;Name=STRG.10035.5.p2;Parent=STRG.10035.5.p2.mrna1;Target=STRG.10035.5.p2 1 346 -
#chr2_RagTag_pilon_pilon_pilon	GenRef	CDS	228562	228593	100	-	0	ID=STRG.10035.5.p2.mrna1.cds1;Name=STRG.10035.5.p2;Parent=STRG.10035.5.p2.mrna1;Target=STRG.10035.5.p2 347 378 .
#chr2_RagTag_pilon_pilon_pilon	GenRef	CDS	227529	227826	100	-	2	ID=STRG.10035.5.p2.mrna1.cds2;Name=STRG.10035.5.p2;Parent=STRG.10035.5.p2.mrna1;Target=STRG.10035.5.p2 49 346 .

#finalTrss = []
with open(snakemake.output[1], 'w') as handle:
    for trsId in keep:
        indelCnt, mismatchCnt = dTrss[trsId]
        if indelCnt == 0 and mismatchCnt <= 1:
            #finalTrss.append(trsId)
            handle.write("%s\n" %trsId)

#with open(snakemake.output[0], 'w') as handle:
#    for trsId in keep:
#        indelCnt, mismatchCnt = dTrss[trsId]
#        if indelCnt == 0 and mismatchCnt <= 1:
#            handle.write("%s\n" %'\n'.join(dTrainingCdss[trsId]))
#        #handle.write("%s\n" %trsId)
shell(
    "fgrep -w -f {snakemake.output[1]} {gff} > {snakemake.output[0]}"
)

#def do_something(data_path, out_path, threads, myparam):
#        # python code#
#
#    do_something(snakemake.input[0], snakemake.output[0], snakemake.threads, snakemake.config["myparam"])
    
#for hdr in fasta.keys():
#    trsId, cdsHit = hdr.split()
#    trsId = trsId.split(".mrna")[0]
#    start, end = cdsHit.replace("CDS=", "").split('-')
#    if int(end) - int(start) + 1 == dInfo[trsId]:
#        resStr += "%s\n" %trsId
