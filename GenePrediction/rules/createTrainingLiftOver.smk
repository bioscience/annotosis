liftOverGtfFile = []
try:
    if config["trainingData"]["liftOverGenePred"]:
        liftOverGtfFile.append("{basename}/liftOver.gtf".format(basename=os.path.basename(config["workDir"])))
except KeyError:
    pass

rule createTrainingLiftOver:
    input:
        reference = config["reference"],
        gtf=liftOverGtfFile
    output:
        cds="{basename}/liftOver.cds.fa".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/gffread.yaml"
    params:
        minIntronSize=config["minIntronSize"],
        maxIntronSize=config["maxIntronSize"]
    log:
        "logs/training/createLiftOver.log"
    #threads: workflow.cores
    shell:
        "(rm -f {input.reference}.fai && gffread {input.gtf} -g {input.reference} -w {output}) > {log}"

