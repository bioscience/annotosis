rule createTrainingSet:
    input:
        ptss="{basename}/gmap.pts.fasta".format(basename=os.path.basename(config["workDir"])),
        cdss="{basename}/nr.cds.fasta".format(basename=os.path.basename(config["workDir"])),
        gffInput="{basename}/gmap.gff".format(basename=os.path.basename(config["workDir"])),
        #reference=config["reference"]
    output:
        gffOutput="{basename}/training.gff".format(basename=os.path.basename(config["workDir"])),
        trssIds=temp("{basename}/trss.ids".format(basename=os.path.basename(config["workDir"])))
        #fasta="training.fasta"
    conda:
        "../envs/pyfaidx.yaml"
    log:
        "logs/createTrainingSet/createTrainingSet.log"
    params:
        minPtsLen=config["minTrainingPtsLen"],
        minExonSize=config["minExonSize"],
        minIntronSize=config["minIntronSize"]
    threads: workflow.cores
    script:
        "../scripts/createTrainingSet.py"
