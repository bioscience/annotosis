rule transeq:
    input:
        "{basename}/gmap.cds.fasta".format(basename=os.path.basename(config["workDir"]))
    output:
        "{basename}/gmap.pts.fasta".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/emboss.yaml"
    log:
        "logs/transeq/transeq.log"
    #params:
    #    extra="--conservative"
    #threads: workflow.cores
    shell:
        "(transeq {input} {output}) > {log}"
