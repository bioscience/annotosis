rule transdecoderPredict:
    input:
        fasta="{basename}/trs99.fasta".format(basename=os.path.basename(config["workDir"])),
        longorfs="trs99.fasta.transdecoder_dir/longest_orfs.pep"
    output:
        cds="trs99.fasta.transdecoder.cds",
        pep="trs99.fasta.transdecoder.pep",
        bed=temp("trs99.fasta.transdecoder.bed"),
        gff3=temp("trs99.fasta.transdecoder.gff3")
    log:
        "logs/transdecoder/predict.log"
    params:
        extra=""
    wrapper:
        "0.68.0/bio/transdecoder/predict"

