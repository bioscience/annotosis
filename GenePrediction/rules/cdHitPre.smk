rule cdHitEstPre:
    input:
        #"trinityGg/Trinity-GG.fasta"
        "{basename}/concatenated.fasta".format(basename=os.path.basename(config["workDir"]))
    output:
        #config["cdhit"]["trs"]
        "{basename}/trs99.fasta".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/cdhit.yaml"
    log:
        "logs/cdhit/cdhitPre.log"
    threads: workflow.cores
    shell:
        "(cd-hit-est -i {input} -o {output} -c 0.99 -M 0 -T {threads}) > {log}"

