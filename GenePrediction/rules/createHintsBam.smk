rnaSeqHintsFile = []
try:
    if config["hintData"]["rnaSeqBamFile"]:
        rnaSeqHintsFile.append(config["hintData"]["rnaSeqBamFile"])
except KeyError:
    pass

rule createHintsBam:
    input:
        #hintsBam=config["hintData"]["rnaSeqBamFile"]
        hintsBam=rnaSeqHintsFile
    output:
        gffBam="{basename}/hintsBam.gff".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/augustus.yaml"
    params:
        source=config["hintParams"]["bam"]["source"],
	priority=config["hintParams"]["bam"]["priority"],
        minIntronSize=config["minIntronSize"],
        maxIntronSize=config["maxIntronSize"]
    log:
        "logs/hints/hintsBam.log"
    #threads: workflow.cores
    shell:
        """(bam2hints --source={params.source} --priority={params.priority} --minintronlen={params.minIntronSize} --maxintronlen={params.maxIntronSize} --in={input.hintsBam} --out={output.gffBam}) >> {log}"""


