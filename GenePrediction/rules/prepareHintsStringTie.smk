stringTieGff3 = []
try:
    if config["hintData"]["stringTieGff3"]:
        stringTieGff3.append(config["hintData"]["stringTieGff3"])
except KeyError:
    pass

rule prepareHintsStringTie:
    input:
        reference=config["reference"],
        gff3=stringTieGff3
    output:
        psl=temp("{basename}/tmpHintsStringTie.psl".format(basename=os.path.basename(config["workDir"]))),
        pslSorted="{basename}/hintsStringTie.psl".format(basename=os.path.basename(config["workDir"])),
        ref2bit=temp("{basename}/refHintsStringTie.2bit".format(basename=os.path.basename(config["workDir"]))),
        refChromSizes=temp("{basename}/refHintsStringTie.chrom.sizes".format(basename=os.path.basename(config["workDir"])))
    conda:
        "../envs/gff3ToPsl.yaml"
    #log:
    #    index="logs/stringtie/stringtieHints.log",
    #threads: workflow.cores
    shell:
        "faToTwoBit {input.reference} {output.ref2bit} && "
        "twoBitInfo {output.ref2bit} stdout | sort -k2,2nr > {output.refChromSizes} && "
        "gff3ToPsl {output.refChromSizes} {targetChromSizes} {input.gff3} {output.psl} /dev/null && "
        "sort -n -k 16,16 {output.psl} | sort -s -k 14,14 > {output.pslSorted}"

#gff3ToPsl - convert a GFF3 CIGAR file to a PSL file
#usage:
#   gff3ToPsl [options] queryChromSizes targetChomSizes inGff3 out.psl
#   arguments:
#      queryChromSizes file with query (main coordinates) chromosome sizes  . File formatted:  chromeName<tab>chromSiz#
#      targetChromSizes file with target (Target attribute)  chromosome sizes .
#      inGff3     GFF3 formatted file with Gap attribute in match records
#      out.psl    PSL formatted output
#   options:
#      -dropQ     drop record when query not found in queryChromSizes
#      -dropT     drop record when target not found in targetChromSizes