stringTieGff3 = []
try:
    if config["hintData"]["stringTieGff3"]:
        stringTieGff3.append(config["hintData"]["stringTieGff3"])
except KeyError:
    pass

rule createHintsStringTie:
    input:
        hintsStringTie=stringTieGff3
        #hintsStringTie="{basename}/hintsStringTie.psl".format(basename=os.path.basename(config["workDir"]))
    output:
        gffSt="{basename}/hintsStringTie.gff".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/augustus.yaml"
    params:
        source=config["hintParams"]["stringTie"]["source"],
	priority=config["hintParams"]["stringTie"]["priority"],
        minIntronSize=config["minIntronSize"],
        maxIntronSize=config["maxIntronSize"]
    log:
        "logs/hints/hintsStringTie.log"
    #threads: workflow.cores
    shell:
        """(exonerate2hints.pl --source={params.source} --priority={params.priority} --minintronlen={params.minIntronSize} --maxintronlen={params.maxIntronSize} --in={input} --out={output}) >> {log}"""


