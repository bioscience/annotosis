rule aug2gff:
    input:
        resDir="augRes",
	reference=config["reference"]
    output:
        gff="{basename}/aug.gff3".format(basename=os.path.basename(config["resultDir"])),
        cds="{basename}/aug.cds.fa".format(basename=os.path.basename(config["resultDir"])),
        trs="{basename}/aug.trs.fa".format(basename=os.path.basename(config["resultDir"])),
        pts="{basename}/aug.pts.fa".format(basename=os.path.basename(config["resultDir"])),
    conda:
        "../envs/convertAug.yaml"
    #params:
    #    config["minIntronSize"]
    #    minPtsLen=config["minTrainingPtsLen"],
    #    minExonSize=config["minExonSize"],
    #log:
    #    "logs/hints/hints.log",
    threads: workflow.cores
    shell:
        #"gffread {input.resDir}/predictions/augustus.gff3 -g {input.reference} -x {output.cds} && "
        #"gffread {input.resDir}/predictions/augustus.gff3 -g {input.reference} -w {output.trs} && "
        #"gffread {input.resDir}/predictions/augustus.gff3 -g {input.reference} -y {output.pts}"
        "python scripts/aug2gff3.py -i {input.resDir}/predictions/augustus.gff -o {output.gff} && "
        "gffread {output.gff} -g {input.reference} -x {output.cds} && "
        "gffread {output.gff} -g {input.reference} -w {output.trs} && "
        "gffread {output.gff} -g {input.reference} -y {output.pts}"
        #"python scripts/aug2gff3.py -i {input.resDir}/predictions/augustus.gff -o {output.gff} && "


