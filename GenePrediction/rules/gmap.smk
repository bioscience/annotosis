rule gmap:
    input:
        reference=config["reference"],
        nr="{basename}/nr.cds.fasta".format(basename=os.path.basename(config["workDir"])),
    output:
        "{basename}/gmap.gff".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/gmap.yaml"
    params:
        config["minIntronSize"]
    log:
        index="logs/gmap/gmapIndex.log",
        align="logs/gmap/gmapAlign.log"
    threads: workflow.cores
    shell:
        """(gmap_build -d GenRef {input.reference}) 2>&1 > {log.index} && """
	"""(gmap -f 2 -B 5 -t {threads} -n 1 --no-chimeras --nofails --min-intronlength={params} -d GenRef {input.nr} > {output}) 2> {log.align}"""


