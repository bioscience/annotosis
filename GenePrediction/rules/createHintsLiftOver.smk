rule createHintsLiftOver:
    input:
        hintsLiftOver="{basename}/hintsLiftOver.psl".format(basename=os.path.basename(config["workDir"]))
    output:
        #gffLo="{basename}/hintsLiftOver.gff".format(basename=os.path.basename(config["workDir"]))
        gffLo="{basename}/hints{group}.gff".format(basename=os.path.basename(config["workDir"]), group="LiftOver")
    conda:
        "../envs/augustus.yaml"
    params:
        source=config["hintParams"]["liftOver"]["source"],
	priority=config["hintParams"]["liftOver"]["priority"],
        minIntronSize=config["minIntronSize"],
        maxIntronSize=config["maxIntronSize"]
    log:
        "logs/hints/hintsLiftOver.log"
    #threads: workflow.cores
    shell:
        """(blat2hints.pl --source={params.source} --priority={params.priority} --minintronlen={params.minIntronSize} --maxintronlen={params.maxIntronSize} --in={input.hintsLiftOver} --out={output.gffLo}) >> {log}"""



