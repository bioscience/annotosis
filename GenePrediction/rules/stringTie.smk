rnaSeqBamFile = []
try:
    if config["hintData"]["rnaSeqBamFile"] and (config["trainingData"]["stringTie"] or config["hintData"]["stringTie"]):
        rnaSeqBamFile.append(config["hintData"]["rnaSeqBamFile"])
except KeyError:
    pass

rule stringTie:
    input:
        rnaSeqBamFile
    output:
        "{basename}/stringTie.gtf".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/stringTie.yaml"
    log:
        "logs/stringtie/stringtie.log"
    #params:
    #    extra="--conservative"
    threads: workflow.cores
    shell:
        "(stringtie -o {output} -p {threads} {input}) > {log}"
