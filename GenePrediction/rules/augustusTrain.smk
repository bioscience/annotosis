rule augustusTraining:
    input:
        genbank="{basename}/training.genbank".format(basename=os.path.basename(config["workDir"])),
        fasta="{basename}/training.fna".format(basename=os.path.basename(config["workDir"]))
        #reference=config["reference"],
    output:
        configPath=directory(config["augustusConfigPath"]),
        augTrainDir=directory("autoAugTrain")
    conda:
        "../envs/augustus.yaml"
    log:
        "logs/augustus/training.log"
    params:
        config["species"]
    threads: workflow.cores
    shell:
        #"""awk 'BEGIN{{cnt=1}}{{if (/>/) {{print ">Scaffold"cnt; cnt+=1}} else print $0}}' {input.reference} > {output.genome} && """
        #"""awk 'BEGIN{{cnt=1}}{{if (/>/) {{print $0"\\tScaffold"cnt; cnt+=1;}} }}' {input.reference} | sed 's/>//' > {output.map} && """
        #"""awk -F"\\t" 'BEGIN{{while ((getline < "{output.map}") > 0) map[$1]=$2;}}{{$1=map[$1]; print $0;}}' {input.gff} | sed 's/ /\\t/g' > {output.training} && """
        "rm -rf $AUGUSTUS_CONFIG_PATH/species/{params} && rm -rf {output.configPath} && "
        "rm -rf {output.augTrainDir} && "
        "mkdir -p {output.configPath} && cp -r $AUGUSTUS_CONFIG_PATH/* {output.configPath} && "
        "export AUGUSTUS_CONFIG_PATH={output.configPath} && "
        #"echo $AUGUSTUS_CONFIG_PATH && "
        #"(autoAugTrain.pl --cpus={threads} --species={params} --trainingset={input.genbank} --genome={output.genome}) > {log}"
        "(autoAugTrain.pl --cpus={threads} --optrounds=4 --species={params} --trainingset={input.genbank} --genome={input.fasta}) > {log}"
	
# simplifyFastaHeaders.pl eco.scafs.fna Scaffold GenePreds/genome.fa GenePreds/genome.map
# sed 's/>//g' genome.map > genome.rn.map
# python ~/Codebase/mapper.py -i export.gff -m genome.rn.map -s "       " > export.rn.gff
# rm -rf /usr/local/augustus/3.1/config/species/egr
# rm -rf autoAugTrain/
# autoAugTrain.pl --species=egr --trainingset=export.rn.gff --genome=genome.fa
#
#(augustus) [pakorhon@macropus scripts]$ ./autoAug.pl --help
#
#Function: train AUGUSTUS and run AUGUSTUS completely and automatically
#
#autoAugTrain.pl [OPTIONS] --trainingset=training.gb --species=sname
#autoAugTrain.pl [OPTIONS] --genome=genome.fa --trainingset=training.gff --species=sname
#autoAugTrain.pl [OPTIONS] --genome=genome.fa --trainingset=training.fa --species=sname
#autoAugTrain.pl [OPTIONS] --genome=genome.fa --species=sname --utr --est=cdna.f.psl --aug=augustus.gff --workingdir=autoTrainNum
#
#--trainingset=training.gb      training.gb is a file with training gene structures in Genbank format
#--trainingset=training.gff     training.gff is a file with training genes in GFF format
#
#--trainingset=training.fa      training.fa is a file with training protein sequences in FASTA format
#
#--genome=genome.fa             FASTA file with DNA sequences for training
#                               genome.fa should include the corresponding sequences in this case
#
#--species=sname                species name as used by AUGUSTUS
#
#--estali=cdna.f.psl            EST alignments are used to guess the UTR and its end point.
#
#--utr                          Switch it on to train AUGUSTUS with untranslated regions. Off by default
#
#options:
#
#--flanking_DNA=length          flanking_DNA length, default:4000
#--workingdir=/path/to/wd/      In the working directory results and temporary files are stored.
#                               Default: current working directory
#                               By case with "utr", the directory "autoTrainRandomNumber" which made by autoAugTrain.pl
#                               without "utr" is expected.
#--verbose                      increase the verbosity level of the program by 1 each, default: 1, max level: 3, e.g. say -v -v -v
#--useexisting                  use and change the present config and parameter files if they exist for 'species'
#--optrounds=n                  optimization rounds - each meta parameter is optimized this often (default 1)
#--CRF                          try training as Conditional Random Field. Off by default
#--aug=augustus.gff             Previous CDS predictions for constructing a training set of UTRs.
#--cpus=n                       n is the number of CPUs to use (default: 1), if cpus > 1 install pblat (parallelized blat) for better performance
