rule augustusPredict:
    input:
        hints="{basename}/aggregatedHints.gff".format(basename=os.path.basename(config["workDir"])),
        reference=config["reference"],
        configPath=config["augustusConfigPath"],
        extrinsic=config["extrinsicFile"]
    output:
        augRes=directory("augRes")
    conda:
        "../envs/augustusParallel.yaml"
    log:
        "logs/augustus/predict.log"
    params:
        species=config["species"],
        augustus=config["augustusParams"]
    threads: workflow.cores
    shell:
        "rm -rf {output.augRes} && "
        "export AUGUSTUS_CONFIG_PATH={input.configPath} && "
        #"""(autoAugPred.pl --cpus={threads} --species={params.species} --augustusoptions="{params.augustus}" --extrinsiccfg={input.extrinsic} --hints={input.hints} --genome={input.reference} --predictionresultsdir={output}) > {log} && """
        """(autoAugPred.pl --cpus={threads} --species={params.species} --augustusoptions="{params.augustus} --extrinsicCfgFile=../../{input.extrinsic} --hintsfile=../../{input.hints}" --genome={input.reference} --predictionresultsdir={output}) > {log} && """
	"""rm -f {output.augRes}/shells/runs.sh && cd {output.augRes}/shells/ && ls aug* | while read line; do echo "./"$line >> runs.sh; done && """
        #"cat runs.sh && ls -ltrh . && "
	"parallel -j {threads} < runs.sh && cd - && "
        """(autoAugPred.pl --continue --species={params.species} --augustusoptions="{params.augustus} --extrinsicCfgFile=../../{input.extrinsic} --hintsfile=../../{input.hints}" --genome={input.reference} --predictionresultsdir={output}) > {log}"""

#Usage:
#
#autoAugPred.pl [OPTIONS] --genome=genome.fa --species=sname
#autoAugPred.pl [OPTIONS] --genome=genome.fa --species=sname --hints=hintsfile
#
#options:
#--genome=genome.fa             fasta file with DNA sequences for training
#--species=sname                species name as used by AUGUSTUS
#--continue                     after cluster jobs are finished, continue to compile prediction sets
#--useexisting                  use and change the present config and parameter files if they exist for 'species'
#--singleCPU                    run sequentially on a single CPU instead of parallel jobs on a cluster
#--cpus=n                       n is the number of CPUs to use (default: 1), if cpus > 1 install Parallel::ForkManager for better performance
#--noninteractive               for Sun Grid Engine users, who have configurated an openssh key
#                               with this option AUGUSTUS is executed automatically on the SGE cluster
#--workingdir=/path/to/wd/      in the working directory results and temporary files are stored.
#--utr                          switch it on to run AUGUSTUS with untranslated regions. Off by default
#--hints=hintsfile              run AUGUSTUS using extrinsic information from hintsfile
#--extrinsiccfg=hintcfgfile     configuration file with parameters (boni/mali) for hints.
#                               default $AUGUSTUS_CONFIG_PATH/config/extrinsic/extrinsic.cfg
#--verbose                      the verbosity level
#--remote=clustername           specify the SGE cluster name for noninteractive, default "fe"
#--AUGUSTUS_CONFIG_PATH=path    path to augustus/config directory. default: use environment variable
#--augustusoptions=options      options to run AUGUSTUS with, the options --utr, --hints and --extrinsiccfg must not be specified
#--predictionresultsdir=dir     the directory were the prediction results are stored, if not set a default folder is used depending on utr and hints settings
#--help                         print this usage info
