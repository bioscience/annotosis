liftOverGenePredFile = []
try:
    if config["trainingData"]["liftOverGenePred"]:
        liftOverGenePredFile.append(config["trainingData"]["liftOverGenePred"])
except KeyError:
    pass

rule prepareTrainingLiftOver:
    input:
        genePred=liftOverGenePredFile
    output:
        gtf="{basename}/liftOver.gtf".format(basename=os.path.basename(config["workDir"])),
    conda:
        "../envs/genepredtogtf.yaml"
    log:
        index="logs/training/prepareLiftOver.log",
    shell:
        "(genePredToGtf file {input} {output}) > {log}"

#genePredToGtf - Convert genePred table or file to gtf.
#usage:
#   genePredToGtf database genePredTable output.gtf
#   If database is 'file' then track is interpreted as a file
#   rather than a table in database.
#   options:
#      -utr - Add 5UTR and 3UTR features
#      -honorCdsStat - use cdsStartStat/cdsEndStat when defining start/end codon records
#      -source=src set source name to use
#      -addComments - Add comments before each set of transcript records.
#                     allows for easier visual inspection
#       Note: use a refFlat table or extended genePred table or file to include
#       the gene_name attribute in the output.  This will not work with a refFlat
#       table dump file. If you are using a genePred file that starts with a numeric
#       bin column, drop it using the UNIX cut command: cut -f 2- in.gp | genePredToGtf file stdin out.gp
			   