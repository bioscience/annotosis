rule createHintsTranscripts:
    input:
        hintsGmap="{basename}/hintsTranscripts.psl".format(basename=os.path.basename(config["workDir"]))
    output:
        gffGm="{basename}/hintsGmap.gff".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/augustus.yaml"
    params:
        source=config["hintParams"]["transcripts"]["source"],
	priority=config["hintParams"]["transcripts"]["priority"],
        minIntronSize=config["minIntronSize"],
        maxIntronSize=config["maxIntronSize"]
    log:
        "logs/hints/hintsTranscripts.log"
    #threads: workflow.cores
    shell:
        """(blat2hints.pl  --source={params.source} --priority={params.priority} --minintronlen={params.minIntronSize} --maxintronlen={params.maxIntronSize} --in={input.hintsGmap} --out={output.gffGm}) >> {log}"""



