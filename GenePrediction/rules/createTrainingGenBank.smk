rule createTrainingGenBank:
    input:
        reference=config["reference"],
        gff="{basename}/training.gff".format(basename=os.path.basename(config["workDir"]))
    output:
        "{basename}/training.genbank".format(basename=os.path.basename(config["workDir"])),
        "{basename}/training.fna".format(basename=os.path.basename(config["workDir"])),
        temp("{basename}/gene.fna".format(basename=os.path.basename(config["workDir"]))),
        temp("{basename}/gene.gff".format(basename=os.path.basename(config["workDir"]))),
        temp("{basename}/gene.genbank".format(basename=os.path.basename(config["workDir"])))
    conda:
        #"../envs/pyfaidx.yaml"
        "../envs/augustus.yaml"
    log:
        "logs/createTrainingSet/createTrainingGenBank.log"
    #params:
    #    minPtsLen=config["minTrainingPtsLen"],
    #    minExonSize=config["minExonSize"],
    #    minIntronSize=config["minIntronSize"]
    threads: workflow.cores
    script:
        "../scripts/gff2genbank.py"
