rule createHintsLongReads:
    input:
        hintsGmap="{basename}/hintsLongReads.psl".format(basename=os.path.basename(config["workDir"]))
    output:
        gffLr="{basename}/hintsLongReads.gff".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/augustus.yaml"
    params:
        source=config["hintParams"]["longReads"]["source"],
	priority=config["hintParams"]["longReads"]["priority"],
        minIntronSize=config["minIntronSize"],
        maxIntronSize=config["maxIntronSize"]
    log:
        "logs/hints/hintsLongReads.log"
    #threads: workflow.cores
    shell:
        """(blat2hints.pl --source={params.source} --priority={params.priority} --minintronlen={params.minIntronSize} --maxintronlen={params.maxIntronSize} --in={input.hintsGmap} --out={output.gffLr}) >> {log}"""



