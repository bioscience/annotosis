#def aggregateHintFiles(wildcards):
#    return expand("{basename}/hints{{hintSources}}.gff".format(basename=os.path.basename(config["workDir"])), hintSources=glob_wildcards(os.path.join("{basename}".format(basename=os.path.basename(config["workDir"])), 'hints{hintSources}.gff')).hintSources)
#    
#rule aggregateHints:
#    input:
#        aggregateHintFiles
#    output:
#        "{basename}/aggregatedHints.gff".format(basename=os.path.basename(config["workDir"]))
#    conda:
#        "../envs/augustus.yaml"
#    log:
#        "logs/hints/hintsAggregate.log"
#    shell:
#        "(cat {input} > {output}) > {log}"""

liftOverGenePredHints = []
try:
    if config["hintData"]["liftOverGenePred"]:
        liftOverGenePredHints.append("{basename}/hintsLiftOver.gff".format(basename=os.path.basename(config["workDir"])))
except KeyError:
    pass

transcriptGmapHints = []
try:
    if config["hintData"]["trinityGgCds"]:
        transcriptGmapHints.append("{basename}/hintsGmap.gff".format(basename=os.path.basename(config["workDir"])))
except KeyError:
    try:
        if config["hintData"]["rnaSeqBamFile"]: # Causes stringTie transcript extraction
            transcriptGmapHints.append("{basename}/hintsGmap.gff".format(basename=os.path.basename(config["workDir"])))
    except KeyError:
        pass

swissProtHints = []
try:
    if config["hintData"]["exonerateSwissProt"]:
        swissProtHints.append("{basename}/hintsSwissProt.gff".format(basename=os.path.basename(config["workDir"])))
except KeyError:
    pass

rnaSeqBamHints = []
try:
    if config["hintData"]["rnaSeqBamFile"]:
        rnaSeqBamHints.append("{basename}/hintsBam.gff".format(basename=os.path.basename(config["workDir"])))
except KeyError:
    pass

longReadHints = []
try:
    if config["hintData"]["longReadTrs"]:
        longReadHints.append("{basename}/hintsLongReads.gff".format(basename=os.path.basename(config["workDir"])))
except KeyError:
        pass

stringTieHints = []
try:
    if config["hintData"]["stringTieGenePred"]:
        stringTieHints.append("{basename}/hintsStringTie.gff".format(basename=os.path.basename(config["workDir"])))
except KeyError:
    pass

rule aggregateHints:
    input:
        #expand("{basename}/hints{{group}}.gff".format(basename=os.path.basename(config["workDir"])))
        gffLo=liftOverGenePredHints,
        gffGm=transcriptGmapHints,
        gffPts=swissProtHints,
        gffBam=rnaSeqBamHints,
	gffLr=longReadHints,
	gffSt=stringTieHints
    output:
        gff="{basename}/aggregatedHints.gff".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/augustus.yaml"
    log:
        "logs/hints/hintsAggregate.log"
    shell:
         "(cat {input.gffLo} {input.gffGm} {input.gffPts} {input.gffBam} {input.gffLr} {input.gffSt} > {output}) >> {log}"""
 