rule redundancy:
    input:
        "{basename}/trs.cds.fasta".format(basename=os.path.basename(config["workDir"]))
    output:
        nr="{basename}/nr.cds.fasta".format(basename=os.path.basename(config["workDir"])),
	tmp=temp("cdhit/tmp.fa"),
    conda:
        "../envs/cdhit.yaml"
    params:
        config["redundancy"]
    log:
        "logs/cdhit/redundancy.log"
    threads: workflow.cores
    shell:
        """awk '{{if ((NR>1)&&($0~/^>/)) {{printf("\\n%s", $0);}} else if (NR==1) {{printf("%s", $0);}} else {{printf("\\t%s", $0);}}}}' {input} | """
	"""grep -F "complete" - | tr "\\t" "\\n" > {output.tmp} && """
	"""(cd-hit-est -i {output.tmp} -o {output.nr} -c {params} -M 0 -T {threads}) > {log}"""
