trinityGgCdsFile = []
try:
    if config["hintData"]["trinityGgCds"]:
        trinityGgCdsFile.append(config["hintData"]["trinityGgCds"])
except KeyError:
    pass

stringTieCdsFile = []
try:
    if config["hintData"]["rnaSeqBamFile"] and config["hintData"]["stringTie"]:
        stringTieCdsFile.append("{basename}/stringTie.cds.fa".format(basename=os.path.basename(config["workDir"])))
except KeyError:
    pass

rule prepareHintsTranscripts:
    input:
        reference=config["reference"],
        #trss="{basename}/trs99.fasta".format(basename=os.path.basename(config["workDir"]))
        #trss="{basename}/concatenated.fasta".format(basename=os.path.basename(config["workDir"]))
        trsTrinityGg=trinityGgCdsFile,
        trsStringTie=stringTieCdsFile
    output:
        psl=temp("{basename}/tmp.psl".format(basename=os.path.basename(config["workDir"]))),
        pslSorted="{basename}/hintsTranscripts.psl".format(basename=os.path.basename(config["workDir"])),
        trss="{basename}/hintsTranscripts.cds.fa".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/gmap.yaml"
    params:
        config["minIntronSize"]
    #    minPtsLen=config["minTrainingPtsLen"],
    #    minExonSize=config["minExonSize"],
    log:
        index="logs/gmap/gmapIndexTrss.log",
        align="logs/gmap/gmapAlignTrss.log"
    threads: workflow.cores
    shell:
        """cat {input.trsTrinityGg} {input.trsStringTie} > {output.trss} && """
        """(gmap_build -d GenRef {input.reference}) 2>&1 > {log.index} && """
	"""(gmap -f 1 -B 5 -t {threads} -n 1 --no-chimeras --nofails --min-intronlength={params} -d GenRef {output.trss} > {output.psl}) 2> {log.align} && """
        """sort -n -k 16,16 {output.psl} | sort -s -k 14,14 > {output.pslSorted}"""
