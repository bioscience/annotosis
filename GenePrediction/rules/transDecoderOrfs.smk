rule transdecoderLongorfs:
    input:
        fasta="{basename}/trs99.fasta".format(basename=os.path.basename(config["workDir"]))
    output:
        "trs99.fasta.transdecoder_dir/longest_orfs.pep"
        #"{basename}.transdecoder_dir/longest_orfs.pep".format(basename=os.path.basename(input.fasta))
    log:
        "logs/transdecoder/longorfs.log"
    params:
        extra=""
    wrapper:
        "0.68.0/bio/transdecoder/longorfs"

