rule gffreadNr:
    input:
        gff="{basename}/gmap.gff".format(basename=os.path.basename(config["workDir"])),
        reference=config["reference"]
    output:
        "{basename}/gmap.cds.fasta".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/gffread.yaml"
    log:
        "logs/gffread/gffreadNr.log"
    #params:
    #    extra="--conservative"
    #threads: workflow.cores
    shell:
        "(rm -f {input.reference}.fai && gffread {input.gff} -g {input.reference} -w {output}) > {log}"
