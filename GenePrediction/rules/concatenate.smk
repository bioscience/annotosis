trinityGgCdsFile = []
try:
    if config["trainingData"]["trinityGgCds"]:
        trinityGgCdsFile.append(config["hintData"]["trinityGgCds"])
except KeyError:
    pass

stringTieCdsFile = []
try:
    if config["hintData"]["rnaSeqBamFile"] and config["trainingData"]["stringTie"]:
        stringTieCdsFile.append("{basename}/stringTie.cds.fa".format(basename=os.path.basename(config["workDir"])))
except KeyError:
    pass

liftOverCdsFile = []
try:
    if config["trainingData"]["liftOverGenePred"]:
        liftOverCdsFile.append("{basename}/liftOver.cds.fa".format(basename=os.path.basename(config["workDir"])))
except KeyError:
    pass

#swissProtCdsFile = []
#try:
#    if config["trainingData"]["exonerateSwissProt"]:
#        swissProtCdsFile.append("{basename}/exonerateSwissProt.cds.fa".format(basename=os.path.basename(config["workDir"])))
#except KeyError:
#    pass

rule concatenate:
    input:
        trinityGgCds=trinityGgCdsFile,
        stringTieCds=stringTieCdsFile,
        liftOverCds=liftOverCdsFile,
        #swissProtCds=swissProtCdsFile
    output:
        "{basename}/concatenated.fasta".format(basename=os.path.basename(config["workDir"]))
    shell:
        #"cat {input.trinityGgCds} {input.stringTieCds} {input.liftOverCds} {input.swissProtCds} > {output}"
        "cat {input.trinityGgCds} {input.stringTieCds} {input.liftOverCds} > {output}"
