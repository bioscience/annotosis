liftOverGenePredFile = []
try:
    if config["hintData"]["liftOverGenePred"]:
        liftOverGenePredFile.append(config["hintData"]["liftOverGenePred"])
except KeyError:
    pass

rule prepareHintsLiftOver:
    input:
        reference=config["reference"],
        genePred=liftOverGenePredFile
    output:
        psl=temp("{basename}/tmpHintsLiftOver.psl".format(basename=os.path.basename(config["workDir"]))),
        pslSorted="{basename}/hintsLiftOver.psl".format(basename=os.path.basename(config["workDir"])),
        ref2bit=temp("{basename}/refHintsLiftOver.2bit".format(basename=os.path.basename(config["workDir"]))),
        refChromSizes=temp("{basename}/refHintsLiftOver.chrom.sizes".format(basename=os.path.basename(config["workDir"])))
    conda:
        "../envs/genepredtofakepsl.yaml"
    #params:
    #    config["minIntronSize"]
    #    minPtsLen=config["minTrainingPtsLen"],
    #    minExonSize=config["minExonSize"],
    log:
        index="logs/cgat/cgat.log",
    threads: workflow.cores
    shell:
        #"echo {input.genePred} && "
        #"touch {output.pslSorted} && "
        "faToTwoBit {input.reference} {output.ref2bit} && "
        "twoBitInfo {output.ref2bit} stdout | sort -k2,2nr > {output.refChromSizes} && "
        "genePredToFakePsl -chromSize={output.refChromSizes} noDB {input.genePred} {output.psl} /dev/null && "
        "sort -n -k 16,16 {output.psl} | sort -s -k 14,14 > {output.pslSorted}"
