stringTieGtfFile = []
try:
    if config["hintData"]["rnaSeqBamFile"] and (config["trainingData"]["stringTie"] or config["hintData"]["stringTie"]):
        stringTieGtfFile.append("{basename}/stringTie.gtf".format(basename=os.path.basename(config["workDir"])))
except KeyError:
    pass

rule createTrainingStringTie:
    input:
        gtf=stringTieGtfFile,
        reference=config["reference"]
    output:
        "{basename}/stringTie.cds.fa".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/gffread.yaml"
    log:
        "logs/gffread/gffread.log"
    #params:
    #    extra="--conservative"
    #threads: workflow.cores
    shell:
        "(rm -f {input.reference}.fai && gffread {input.gtf} -g {input.reference} -w {output}) > {log}"
