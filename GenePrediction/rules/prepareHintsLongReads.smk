longReadTrsFile = []
try:
    if config["hintData"]["longReadTrs"]:
        longReadTrsFile.append(config["hintData"]["longReadTrs"])
except KeyError:
    pass

rule prepareHintsLongReads:
    input:
        reference=config["reference"],
        trsLongReads=longReadTrsFile,
    output:
        psl=temp("{basename}/tmp.psl".format(basename=os.path.basename(config["workDir"]))),
        pslSorted="{basename}/hintsLongReads.psl".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/gmap.yaml"
    params:
        config["minIntronSize"]
    #    minPtsLen=config["minTrainingPtsLen"],
    #    minExonSize=config["minExonSize"],
    log:
        index="logs/gmap/gmapIndexTrss.log",
        align="logs/gmap/gmapAlignTrss.log"
    threads: workflow.cores
    shell:
        """(gmap_build -d GenRef {input.reference}) 2>&1 > {log.index} && """
	"""(gmap -f 1 -B 5 -t {threads} -n 1 --no-chimeras --nofails --min-intronlength={params} -d GenRef {input.trsLongReads} > {output.psl}) 2> {log.align} && """
        """sort -n -k 16,16 {output.psl} | sort -s -k 14,14 > {output.pslSorted}"""
