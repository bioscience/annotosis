swissProtHintsFile = []
try:
    if config["hintData"]["exonerateSwissProt"]:
        swissProtHintsFile.append(config["hintData"]["exonerateSwissProt"])
except KeyError:
    pass

rule createHintsSwissProt:
    input:
        swissProtHintsFile
        #exoneratePts=config["hintData"]["exonerateSwissProt"]
    output:
        gffPts="{basename}/hintsSwissProt.gff".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/augustus.yaml"
    params:
        source=config["hintParams"]["swissProt"]["source"],
	priority=config["hintParams"]["swissProt"]["priority"],
        minIntronSize=config["minIntronSize"],
        maxIntronSize=config["maxIntronSize"]
    log:
        "logs/hints/hintsProteins.log"
    #threads: workflow.cores
    shell:
        """(exonerate2hints.pl --source={params.source} --priority={params.priority} --minintronlen={params.minIntronSize} --maxintronlen={params.maxIntronSize} --in={input} --out={output}) > {log}"""


