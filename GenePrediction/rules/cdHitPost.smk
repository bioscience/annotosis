rule cdHitEstPost:
    input:
        "trs99.fasta.transdecoder.cds"
    output:
        "{basename}/trs.cds.fasta".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/cdhit.yaml"
    log:
        "logs/cdhit/cdhitPost.log"
    threads: workflow.cores
    shell:
        "(cd-hit-est -i {input} -o {output} -c 0.99 -M 0 -T {threads}) > {log}"
