# Annotosis Gene Prediction Pipeline

The pipelines for gene prediction are implemented using snakemake.
To install latest snakemake, follow instructions in [Snakemake documentation.](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html).
Annotosis consist of four pipelines. **RnaProcessing** pipeline creates a putatively full-length non-redundant set of CDS transcript (including splicing variants) using genome guided *Trinity*, *CD-HIT* and *TransDecoder* programs; **FastExo** creates hints to Augustus prediction software from large protein databases; **GenePrediction** pipeline uses the resultant CDS transcripts and the BAM file to predict the genes for the given genome in FASTA format; and **Validation** estimates the quality of predicted genes and proposes the ones to keep for further analyses. Transferred genes models from previous assembly/assemblies can be added to the pipeline using the program *LiftOver*.

You can clone the latest version of pipelines using command
```
git clone https://gitlab.unimelb.edu.au/bioscience/annotosis.git
```

### To run RnaProcessing pipeline, edit config.yaml, units.tsv and samples.tsv files and run
```
cd RnaProcessing
snakemake --use-conda --cores N
cd ..
```
where N is the maximum number of threads that can be used by the pipeline.

### To run FastExo, first download SwissProt database, remove sequences containing amino (aa) acids U or O, or sequences or length < 30 aa, and remove redundant sequences.
```
cd FastExo
wget https://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz
gunzip uniprot_sprot.fasta.gz
awk 'ORS="";/>/{$0=s$0"\n";s="\n"}1' uniprot_sprot.fasta | awk '!/>/&&!/U/&&!/O/{if (length($0)>=30) print $0}/>/{print $0}' | awk '{print $1}' | awk 'BEGIN{RS =">";FS="\n";ORS=""}{if ($2) print ">"$0}' | awk '!/^>/{s[$0]=h}/^>/{h=$0}END{for (h in s) print s[h]"\n"h}' > uniprot_sprot.final.fasta
```
Next edit config.yaml file and run
```
snakemake --use-conda --cores N
cd ..
```

### To run gene prediction, edit config.yaml file and run 
```
cd GenePrediction
snakemake --use-conda --cores N
cd ..
```

### For Validator pipeline, first build the containers for singularity and then run
```
cd Containers
singularity build --fakeroot flps.sif flps.def
singularity build --fakeroot genevalidator.sif genevalidator.def
singularity build --fakeroot interpro.sif interpro.def
cd ..
```
or
```
cd Containers
sudo singularity build flps.sif flps.def
sudo singularity build genevalidator.sif genevalidator.def
sudo singularity build interpro.sif interpro.def
cd ..
```
Second edit the paths in config.yaml, and finally run
```
cd Validator
snakemake --use-singularity --singularity-args "-B /home/../Validator" --use-conda --cores N
cd ..
```

### Note that these pipelines are still under development and if you experience difficulties running the pipelines, try
```
snakemake --use-conda --cores 1 --debug --verbose
```
to get more information and/or contact [*pasi.korhonen@unimelb.edu.au*.](mailto:pasi.korhonen@unimelb.edu.au)
