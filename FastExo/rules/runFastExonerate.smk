
rule fastExonerate:
    input:
        pair="FileDir/pair{i}",
        #dnaPair="FileDir/{i}",
        #protPair="FileDir/{i}"
        #dnaPair="FileDir/dnaPair_{i}.fa",
        #protPair="FileDir/protPair_{i}.fa"
        #dnaPair="{basename}/dnaPair_{{i}}.fa".format(basename=os.path.basename(config["resultDir"])),
        #protPair="{basename}/protPair_{i}.fa".format(basename=os.path.basename(config["resultDir"]))
    output:
        exoGff="FileDir/exonerate{i}.gff"
        #exoGff="{basename}/exonerate.gff".format(basename=os.path.basename(config["resultDir"]))
    log:
        "logs/fastExonerate/{i}.log"
    conda:
        "../envs/fastExonerate.yaml"
    params:
        minIntronSize=config["minIntronSize"],
        #fileBundleCnt=config["fileBundleCnt"]
    shell:
         """LINES=`wc -l {input} | awk '{{print $1}}'` && """
         """HALF=$((LINES/2)) && """
         """BLOCKS=$((HALF/2)) && """
         """HCNT=0 && """
         """for i in $(seq 1 1 $BLOCKS); do """
         """  HCNT=$((HCNT+2)) && """
         """  head -n $HCNT {input} | tail -n 2 > {input}.dna && """
         """  TCNT=$((HCNT+HALF)) && """
         """  head -n $TCNT {input} | tail -n 2 > {input}.prot && """
         """  scaf=`head -n 1 {input}.dna | sed 's/>//1' | awk -F"__" '{{print $1}}'` && """
         """  exonerate --minintron {params} --model protein2genome -S FALSE --showtargetgff TRUE --showvulgar FALSE --showalignment no --singlepass FALSE --bestn 1 -q {input}.prot -t {input}.dna >> {output}; """
         """done """


