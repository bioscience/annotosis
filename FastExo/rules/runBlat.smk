swissProtProteinsFile = []
try:
    if config["swissProtProteins"]:
        swissProtProteinsFile.append(config["swissProtProteins"])
except KeyError:
    pass

rule runBlat:
    input:
        exoneratePts=swissProtProteinsFile,
        reference=config["reference"]
    output:
        cleanPts="{basename}/cleaned_proteins.fasta".format(basename=os.path.basename(config["workDir"])),
        pBlatPsl="{basename}/pblat_proteins.psl".format(basename=os.path.basename(config["workDir"])),
        bestPsl="{basename}/best_pblat_proteins.psl".format(basename=os.path.basename(config["workDir"])),
        ptsTab="{basename}/cleaned_proteins.tsv".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/fastExonerate.yaml"
    params:
        maxIntronSize=config["maxIntronSize"]
    log:
        "logs/hints/pblat.log"
    threads: workflow.cores
    shell:
        """awk 'BEGIN{{getline; print $1; seq="";}}{{if($1 ~ "^>"){{print seq; seq=""; print $1;}}else{{seq=seq""$1}}}}END{{print seq}}' {input.exoneratePts} | """
	"""sed 's/|/_/g; s/:/_/g' > {output.cleanPts} 2> {log} && """
        """awk '{{if($1 ~ "^>"){{printf $1"\t"; getline; print $1}}}}' {input.exoneratePts} | sed 's/|/_/g; s/:/_/g' > {output.ptsTab} 2> {log} && """
        """pblat -threads={threads} -q=prot -t=dnax -fine -maxIntron={params.maxIntronSize} {input.reference} {input.exoneratePts} {output.pBlatPsl} >> {log} 2>&1 && """
        """awk -F"\t" '{{if($0 ~ /^psLayout/){{for(i=1; i < 5; i++){{getline;}}}}else{{print $0}}}}' {output.pBlatPsl} | """
	"""sort -t$'\t' -k10,10 -k1,1nr -k15,15nr | sort -u -k10,10 --merge > {output.bestPsl} 2>> {log}"""
