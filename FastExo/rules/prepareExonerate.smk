swissProtProteinsFile = []
try:
    if config["swissProtProteins"]:
        swissProtProteinsFile.append(config["swissProtProteins"])
except KeyError:
    pass

rule prepareExonerate:
    input:
        bestPsl="{basename}/best_pblat_proteins.psl".format(basename=os.path.basename(config["workDir"])),
        ptsTab="{basename}/cleaned_proteins.tsv".format(basename=os.path.basename(config["workDir"])),
        exoneratePts=swissProtProteinsFile,
        reference=config["reference"]
    output:
        coordInfo="{basename}/coord.info.tsv".format(basename=os.path.basename(config["workDir"])),
        matchCoord="{basename}/match_coord.bed".format(basename=os.path.basename(config["workDir"])),
        matchSection="{basename}/match_sections.tsv".format(basename=os.path.basename(config["workDir"])),
        pairingsLookup="{basename}/pairingsLookup.tsv".format(basename=os.path.basename(config["workDir"]))

    conda:
        "../envs/fastExonerate.yaml"
    params:
        maxIntronSize=config["maxIntronSize"]
    log:
        "logs/hints/pblat.log"
    threads: workflow.cores
    shell:
        """cut -f 10,14-17 {input.bestPsl} | """
	"""awk -F"\t" '{{printf $1"\t"$2"\t"$3; if($4-500 < 0){{printf "\t0"}}else{{printf "\t"$4-500}}; if($5+500 > $3-1){{print "\t"$3-1}}else{{print "\t"$5+500}}}}' | """
	"""sed 's/|/_/g' > {output.coordInfo} && """
        """cut -f2,4,5 {output.coordInfo} | sort -k1,1 -k2,2g | uniq > {output.matchCoord} && """
        """bedtools getfasta -fi {input.reference} -bed {output.matchCoord} | sed 's/:/__/' | """
	"""awk '{{if($1 ~ "^>"){{header=$1; getline; seq[header]=$1}}}}END{{for(i in seq){{print i"\t"seq[i]}}}}' >> {output.matchSection} && """
        """awk -F"\t" 'BEGIN{{while(getline < "{input.ptsTab}"){{prots[$1]=$2}}; while(getline < "{output.matchSection}"){{scaffs[$1]=$2}}; }}{{print $0"\t"prots[">"$1]"\t"scaffs[">"$2"__"$4"-"$5]}}' {output.coordInfo} > {output.pairingsLookup}"""

