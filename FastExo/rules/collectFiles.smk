def aggregate_files(wildcards):
    '''
    '''
    checkpointOutput = checkpoints.createInputFiles.get(**wildcards).output[0]
    return expand("FileDir/exonerate{i}.gff", i = glob_wildcards(os.path.join("FileDir", "pair{i}")).i)

rule collectFiles:
    input:
        aggregate_files
    output:
        "WorkDir/exonerate.gff"
        #"{basename}/exonerate.gff".format(basename=os.path.basename(config["resultDir"]))
    shell:
         """cat {input} > {output}"""

