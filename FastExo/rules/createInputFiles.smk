
checkpoint createInputFiles:
    input:
        pairingsLookup="{basename}/pairingsLookup.tsv".format(basename=os.path.basename(config["workDir"])),
    output:
        directory("FileDir")
        #directory(config["workDir"]),
    params:
        fileBundleCnt=config["fileBundleCnt"]
    #shell:
    #    """set i=1 && """
    #    """cat {input} | while read line; do awk -F"\t" '{{print ">"$2"__"$4"-"$5"\\n"$NF}}' > {output}/dnaPair_$i.fa; ((i++)); done && """
    #    """set i=1 && """
    #    """cat {input} | while read line; do awk -F"\t" '{{print ">"$1"\\n"$(NF-1)}}' > {output}/protPair_$i.fa; ((i++)); done """
    script:
        "../scripts/createInputFiles.py"
