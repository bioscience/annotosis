
rule cleanGffFile:
    input:
        gff="{basename}/exonerate.gff".format(basename=os.path.basename(config["workDir"]))
    output:
        gff="{basename}/exonerateClean.gff".format(basename=os.path.basename(config["workDir"])),
    script:
        "../scripts/cleanGff.py"

