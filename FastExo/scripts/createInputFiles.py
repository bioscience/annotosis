from snakemake.shell import shell
import re

# Placeholder for optional parameters
fileBundleCnt = int(snakemake.params.get("fileBundleCnt", ""))
#baseDir = int(snakemake.params.get("baseDir", ""))
# Run log
#log = snakemake.log_fmt_shell()

# Placeholder for optional parameters
pairs = snakemake.input.get("pairingsLookup")
#baseDir = snakemake.input.get("baseDir")
baseDir = snakemake.output[0]

shell("mkdir -p %s" %baseDir)

i = 1
cnt = 1
bundleDna, bundleProt = [], []
with open(pairs) as handle:
    for line in handle:
        items = line.strip().split('\t')
        if len(items) < 7: continue # Expect both protein and dna sequences
        bundleDna.append(">%s__%s-%s\n%s\n" %(items[1], items[3], items[4], items[-1]))
        bundleProt.append(">%s\n%s\n" %(items[0], items[-2]))
        if i % fileBundleCnt == 0:
            with open("%s/pair_%d.fa" %(baseDir, cnt), 'w') as handle:
                for seq in bundleDna:
                    handle.write(seq)
                for seq in bundleProt:
                    handle.write(seq)
            bundleDna, bundleProt = [], []
            cnt += 1
        i += 1
    if len(bundleDna) > 0:
        with open("%s/pair_%d.fa" %(baseDir, cnt), 'w') as handle:
            for seq in bundleDna:
                handle.write(seq)
            for seq in bundleProt:
                handle.write(seq)

#with open(snakemake.output[0], 'w') as handle:
#    for trsId in keep:
#        indelCnt, mismatchCnt = dTrss[trsId]
#        if indelCnt == 0 and mismatchCnt <= 1:
#            handle.write("%s\n" %'\n'.join(dTrainingCdss[trsId]))
#        #handle.write("%s\n" %trsId)
