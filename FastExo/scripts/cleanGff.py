from snakemake.shell import shell
import re

gff = snakemake.input.get("gff")

geneRecord = []
record = True
geneId = None
cnt = 0
# Extract GFF from exonerate output and modify to work with gffread
gnRe = re.compile(r"^gene_id ([0-9]+) ;")
with open(snakemake.output[0], 'w') as handleW:
    with open("%s" %gff) as handle:
        for line in handle:
            line = line.strip()
            if line == "# --- END OF GFF DUMP ---":
                record = False
            if record == True and line[0] != '#':
                items = line.split('\t')
                if len(items) < 3: continue
                if items[2] == "gene":
                    if geneId != None:
                        handleW.write("%s\n" %'\n'.join(geneRecord))
                        geneRecord = []
                    #handleW.write("%s" %line)
                    geneId = gnRe.search(items[-1]).group(1)
                    cnt += 1
                    items[-1] = items[-1].replace(geneId, '\"%d\"' %cnt, 1)
                    geneId = "%d" %cnt
                    geneRecord.append("%s" %'\t'.join(items))
                elif items[2] == "cds":
                    items.append('transcript_id "%s" ;' %geneId)
                    #geneLen += int(items[4]) - int(items[3]) + 1
                    geneRecord.append("%s" %'\t'.join(items))
                elif items[2] == "exon":
                    items[-1] = 'transcript_id "%s" ; %s' %(geneId, items[-1])
                    geneRecord.append("%s" %'\t'.join(items))
            if line == "# --- START OF GFF DUMP ---":
                record = True
        if geneId != None:
            handleW.write("%s\n" %'\n'.join(geneRecord))

