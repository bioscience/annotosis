
rule gff2asn:
    input:
        gff="{basename}/scafs.gff3".format(basename=os.path.basename(config["workDir"])),
        scafs="{basename}/scafs.fasta".format(basename=os.path.basename(config["workDir"])),
        #scafs=config["scafs"],
        #gff=config["gff"],
        #proteome="FileDir/part{i}.fa",
    output:
        gff=temp("{basename}/tmp.gff".format(basename=os.path.basename(config["workDir"]))),
        dr="{basename}/gff2asn.dr".format(basename=os.path.basename(config["workDir"])),
        val="{basename}/gff2asn.val".format(basename=os.path.basename(config["workDir"])),
        sqn="{basename}/gff2asn.sqn".format(basename=os.path.basename(config["workDir"])),
        gbf="{basename}/gff2asn.gbf".format(basename=os.path.basename(config["workDir"])),
        stats="{basename}/gff2asn.stats".format(basename=os.path.basename(config["workDir"])),
    container:
        config["flps"]
        #"docker://pakorhon/validator:latest"
    #params:
    #    "{basename}/gff2asn".format(basename=os.path.basename(config["workDir"])),
    log:
        "logs/gff2asn/gff2asn.log"
    #threads: workflow.cores
    shell:
        """awk -F["\t","="] '/\tgene\t/{{OFS="\t"; $9=$9"="$10";locus_tag=TAG_"$10;$10=""}}1' {input.gff} > {output.gff} && """
        "table2asn -M n -J -c w -euk -l paired-ends -i {input.scafs} -f {output.gff} -o {output.sqn} -Z -locus-tag-prefix TAG -V b"
