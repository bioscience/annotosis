
rule joinOverlaps:
    input:
        #wd=config["workDir"],
        #overlapGene="{basename}/Overlaps/geneRepeatOverlap.res".format(basename=os.path.basename(config["workDir"])),
        overlapMrna="{basename}/Overlaps/mRNARepeatOverlap.res".format(basename=os.path.basename(config["workDir"])),
        overlapExon="{basename}/Overlaps/exonRepeatOverlap.res".format(basename=os.path.basename(config["workDir"])),
        overlapCds="{basename}/Overlaps/cdsRepeatOverlap.res".format(basename=os.path.basename(config["workDir"])),
    output:
        repOverlaps="{basename}/Overlaps/joined.res".format(basename=os.path.basename(config["workDir"])),
    conda:
        "../envs/bedtools.yaml"
    #params:
    #    "{basename}/kmeans".format(basename=os.path.basename(config["workDir"])),
    log:
        "logs/overlaps/overlaps.log"
    #threads: workflow.cores
    shell:
        """python scripts/joiner.py -i "{input.overlapMrna} {input.overlapExon} {input.overlapCds}" > {output}"""
