def aggregate_files(wildcards):
    '''
    '''
    checkpointOutput = checkpoints.partition.get(**wildcards).output[0]
    return expand("GeneValDirs/summary{i}.tsv", i = glob_wildcards(os.path.join("FileDir", "part{i}.fa")).i)

rule collect:
    input:
        aggregate_files
    output:
        summary="WorkDir/geneValidator.tsv"
        #"{basename}/exonerate.gff".format(basename=os.path.basename(config["resultDir"]))
    shell:
         #"""cat {input} | grep -v "MissingExtraSequences" > {output.summary}"""
         #"""rm -f {output.summary} && """
         #"""for f in {input}; do sed 1,1d $f >> {output.summary}; done"""
         """rm -f {output.summary} && cnt=1 && """
         """for f in {input}; do sed 1,1d $f >> {output.summary}; done"""
         #"""for f in {input}; do sed 1,1d $f | while read line; do sed "s/^[0-9]*/$cnt/1"; done >> {output.summary}; ((cnt++)); done"""
