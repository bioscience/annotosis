
rule buscoBefore:
    input:
        proteome="{basename}/proteome.fa".format(basename=os.path.basename(config["workDir"])),
    output:
        res=directory("buscoBefore"),
        #res=directory("{basename}/buscoBefore".format(basename=os.path.basename(config["workDir"]))),
    container:
        "docker://ezlabgva/busco:v5.1.2_cv1"
    params:
        config["lineage"]
    log:
        "logs/busco/buscoBefore.log"
    threads: workflow.cores
    shell:
        #"(docker run -u $(id -u) -v $(pwd):/busco_wd:Z ezlabgva/busco:v5.1.2_cv1 busco -i /busco_wd/{input} -c {threads} -m geno -l {params} -r -o {output}) > {log}"
        #"(busco -i /busco_wd/{input} -c {threads} -m prot -l {params} -r -o {output}) > {log}"
        "(busco -i {input} -c {threads} -m prot -l {params} -r -o {output}) > {log}"
