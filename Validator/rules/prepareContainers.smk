
rule prepareContainers:
    input:
        proteome=config["proteome"],
        gff=config["gff"],
        scafs=config["scafs"],
    output:
        proteome="{basename}/proteome.fa".format(basename=os.path.basename(config["workDir"])),
        gff="{basename}/scafs.gff3".format(basename=os.path.basename(config["workDir"])),
        scafs="{basename}/scafs.fasta".format(basename=os.path.basename(config["workDir"])),
    shell:
        "cp -f {input.proteome} {output.proteome} && cp -f {input.gff} {output.gff} && cp -f {input.scafs} {output.scafs}"
