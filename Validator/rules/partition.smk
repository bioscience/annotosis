
checkpoint partition:
    input:
        proteome=config["proteome"],
    output:
        directory("FileDir")
    params:
        seqCnt=config["seqCnt"]
    #log:
    #    "logs/partition/partition.log"
    shell:
        "mkdir -p {output} && cd {output} && python ../scripts/partition.py -i {input} -n {params}"
