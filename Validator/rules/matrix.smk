def aggregate_files(wildcards):
    '''
    '''
    checkpointOutput = checkpoints.kmeans.get(**wildcards).output[0]
    return expand("Kmeans/kmeans{i}.cluster", i = glob_wildcards(os.path.join("Kmeans", "kmeans{i}.cluster")).i)

rule matrix:
    input:
        aggregate_files
    output:
        idxs="{basename}/gene.idxs".format(basename=os.path.basename(config["workDir"])),
        mat="{basename}/mat.npy".format(basename=os.path.basename(config["workDir"])),
        #clusters="{basename}/clusters.txt".format(basename=os.path.basename(config["workDir"])),
    conda:
        "../envs/python.yaml"
    params:
        wd=config["workDir"]
    shell:
         "python scripts/pairWise.py -d {params.wd} -i '{input}'"
