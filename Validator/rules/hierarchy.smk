rule hierarchy:
    input:
        idxs="{basename}/gene.idxs".format(basename=os.path.basename(config["workDir"])),
        mat="{basename}/mat.npy".format(basename=os.path.basename(config["workDir"])),
    output:
        "{basename}/clusters1.png".format(basename=os.path.basename(config["workDir"])),
        "{basename}/clusters2.png".format(basename=os.path.basename(config["workDir"])),
        "{basename}/dendro.png".format(basename=os.path.basename(config["workDir"])),
        "{basename}/matOrdered.npy".format(basename=os.path.basename(config["workDir"])),
        "{basename}/geneOrdered.idxs".format(basename=os.path.basename(config["workDir"])),
        #mat="{basename}/mat.npy".format(basename=os.path.basename(config["workDir"])),
        #clusters="{basename}/clusters.txt".format(basename=os.path.basename(config["workDir"])),
    conda:
        "../envs/python.yaml"
    params:
        wd=config["workDir"],
        cnt=config["kmeansClusterCnt"]
    shell:
         "python scripts/hierarchy.py -d {params.wd} -i {input.idxs} -m {input.mat} -c {params.cnt}"
