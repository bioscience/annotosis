
rule createData:
    input:
        #wd=config["workDir"],
        gff=config["gff"],
        ipro="{basename}/inter.pro".format(basename=os.path.basename(config["workDir"])),
        repOverlaps="{basename}/Overlaps/joined.res".format(basename=os.path.basename(config["workDir"])),
        mcl="{basename}/OrthoMcl/groups.txt".format(basename=os.path.basename(config["workDir"])),
        flps="{basename}/flps.res".format(basename=os.path.basename(config["workDir"])),
        geneValidator="{basename}/geneValidator.tsv".format(basename=os.path.basename(config["workDir"])),
    output:
        data="{basename}/data.txt".format(basename=os.path.basename(config["workDir"])),
    conda:
        "../envs/bedtools.yaml"
    #params:
    #    "{basename}/kmeans".format(basename=os.path.basename(config["workDir"])),
    #log:
    #    "logs/overlaps/overlaps.log"
    #threads: workflow.cores
    shell:
        "python scripts/filterGenes.py -i {input.gff} -a {input.ipro} -r {input.repOverlaps} -m {input.mcl} -f {input.flps} -g {input.geneValidator} > {output}"
