
rule interpro:
    input:
        proteome="{basename}/proteome.fa".format(basename=os.path.basename(config["workDir"])),
    output:
        tmp=directory("{basename}/tmpInterPro".format(basename=os.path.basename(config["workDir"]))),
        res="{basename}/inter.pro".format(basename=os.path.basename(config["workDir"])),
    container:
        config["interpro"]
        #"file:///home/pakorhon/Eco/PacBio/CanuSingle18/Final/GenePreds/Validator/interpro.sif"
    #params:
    #    "{basename}/kmeans".format(basename=os.path.basename(config["workDir"])),
    log:
        "logs/interpro/interpro.log"
    threads: workflow.cores
    shell:
        "/interproscan-5.51-85.0/interproscan.sh -i {input} -T {output.tmp} -cpu {threads} -f tsv --goterms -dp -o {output.res}"
