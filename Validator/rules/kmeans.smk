
checkpoint kmeans:
    input:
        data="{basename}/data.txt".format(basename=os.path.basename(config["workDir"])),
        #scafs=config["scafs"],
        #proteome="FileDir/part{i}.fa",
    output:
        directory("Kmeans")
        #cluster="{basename}/kmeans.cluster".format(basename=os.path.basename(config["workDir"])),
        #centers="{basename}/kmeans.centers".format(basename=os.path.basename(config["workDir"])),
        #size="{basename}/kmeans.size".format(basename=os.path.basename(config["workDir"])),
    conda:
        "../envs/kmeans.yaml"
    params:
        clusterCnt=config["kmeansClusterCnt"],
	bootCnt=config["bootStrapCnt"],
        baseName="Kmeans/kmeans",
        #baseName="{basename}/kmeans".format(basename=os.path.basename(config["workDir"])),
    log:
        "logs/kmeans/kmeans.log"
    #threads: workflow.cores
    shell:
        "rm -rf Kmeans/ && mkdir -p Kmeans && "
        "for ((i=1;i<={params.bootCnt};i++)); do "
        "Rscript --vanilla scripts/kmeans.r {input.data} {params.clusterCnt} {params.baseName}${{i}}; "
	"done"
