#container: "shub://ISU-HPC/mysql"

# https://www.hpc.iastate.edu/guides/containers/mysql-server
rule orthomcl:
    input:
        proteome=config["proteome"],
	mysql=config["mysql"],
    output:
        res="{basename}/OrthoMcl/groups.txt".format(basename=os.path.basename(config["workDir"])),
    #container: None
    conda:
        "../envs/orthomcl.yaml"
    params:
        wd=config["workDir"],
        socket=config["socket"]
    #log:
    #    "logs/orthomcl/orthomcl.log"
    threads: workflow.cores
    shell:
        #"singularity instance stop mysql && rm -f mysql/run/mysqld/mysqld.pid && rm -f mysql/run/mysqld/mysqld.sock.lock && "
        "rm -f mysql/run/mysqld/mysqld.pid && rm -f mysql/run/mysqld/mysqld.sock.lock && "
        "echo ${{HOME}} && singularity instance start --bind {input.mysql}:${{HOME}} --bind {input.mysql}/var/lib/mysql/:/var/lib/mysql --bind {input.mysql}/run/mysqld:/run/mysqld shub://ISU-HPC/mysql mysql && "
        #"echo ${{HOME}} && singularity instance start --bind {input.mysql}:${{HOME}} --bind {input.mysql}/var/lib/mysql/:/tmp --bind {input.mysql}/run/mysqld:/run/mysqld shub://ISU-HPC/mysql mysql && "
	"singularity run instance://mysql && "
        #"""alias mysql="mysql -S {params.socket}" && python scripts/orthoMcl.py -d {params.wd} -i {input.proteome} -l TMP -T {threads} -S {params.socket} && """
        "python scripts/orthoMcl.py -d {params.wd} -i {input.proteome} -l TMP -T {threads} -S {params.socket} && "
	"singularity instance stop mysql"
