rule plot:
    input:
        #idxs="{basename}/geneOrdered.idxs".format(basename=os.path.basename(config["workDir"])),
        mat="{basename}/data.txt".format(basename=os.path.basename(config["workDir"])),
    output:
        "{basename}/scoresGene.tsv".format(basename=os.path.basename(config["workDir"])),
        "{basename}/scoresGeneSel.tsv".format(basename=os.path.basename(config["workDir"])),
        "{basename}/scoresGene.jpg".format(basename=os.path.basename(config["workDir"])),
        "{basename}/scoresBiplot.jpg".format(basename=os.path.basename(config["workDir"])),
        #"{basename}/kmeansElbow.jpg".format(basename=os.path.basename(config["workDir"])),
        #"{basename}/kmeansHeatmap.jpg".format(basename=os.path.basename(config["workDir"])),
        #"{basename}/bootClusters.tsv".format(basename=os.path.basename(config["workDir"])),
        #"{basename}/biplot.jpg".format(basename=os.path.basename(config["workDir"])),
        #"{basename}/kmeansScatter.jpg".format(basename=os.path.basename(config["workDir"])),
        #"{basename}/kmeansScree.jpg".format(basename=os.path.basename(config["workDir"])),
        #"{basename}/clusters2.png".format(basename=os.path.basename(config["workDir"])),
        #"{basename}/dendro.png".format(basename=os.path.basename(config["workDir"])),
    conda:
        "../envs/kmeans.yaml"
        #"../envs/python.yaml"
    params:
        baseName="{basename}/scores".format(basename=os.path.basename(config["workDir"])),
        cnt=config["kmeansClusterCnt"],
        smoothing=config["smoothing"],
    shell:
         #"python scripts/plot.py -d {params.wd} -i {input.idxs} -m {input.mat} -c {params.cnt}"
         #"Rscript --vanilla scripts/plot.r {input.idxs} {input.mat} {params.cnt} {params.baseName}"
         "Rscript --vanilla scripts/plot.r {input} {params.baseName} {params.cnt} {params.smoothing}"
