
rule overlaps:
    input:
        geneGff=config["gff"],
        repsGff=config["gffReps"],
    output:
        geneGff="{basename}/Overlaps/genes.gff".format(basename=os.path.basename(config["workDir"])),
        overlapGene="{basename}/Overlaps/geneRepeatOverlap.res".format(basename=os.path.basename(config["workDir"])),
        overlapMrna="{basename}/Overlaps/mRNARepeatOverlap.res".format(basename=os.path.basename(config["workDir"])),
        overlapExon="{basename}/Overlaps/exonRepeatOverlap.res".format(basename=os.path.basename(config["workDir"])),
        overlapCds="{basename}/Overlaps/cdsRepeatOverlap.res".format(basename=os.path.basename(config["workDir"])),
        #"{basename}/geneReps.overlaps".format(basename=os.path.basename(config["workDir"])),
    conda:
        "../envs/bedtools.yaml"
        #"file:///home/pakorhon/Eco/PacBio/CanuSingle18/Final/GenePreds/Validator/interpro.sif"
    params:
        wd=config["workDir"],
    #    "{basename}/kmeans".format(basename=os.path.basename(config["workDir"])),
    log:
        "logs/overlaps/overlaps.log"
    #threads: workflow.cores
    shell:
        """grep -v "#" {input.geneGff} > {output.geneGff} && """
        "python scripts/geneRepeatOverlaps.py -d {params.wd} -i {output.geneGff} -r {input.repsGff}"
