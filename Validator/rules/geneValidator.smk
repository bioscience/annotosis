
rule geneValidator:
    input:
        #proteome=config["proteome"],
        proteome="FileDir/part{i}.fa",
	geneValDir="GeneValidator",
    output:
        dirs=directory("GeneValDirs/Val{i}"),
        summary="GeneValDirs/summary{i}.tsv"
    container:
        config["geneValidator"]
    #    "docker://pakorhon/validator:latest"
    params:
        blastDb=config["blastDb"]
    log:
        "logs/genevalidator/geneValidator{i}.log"
    #threads: workflow.cores
    shell:
        #"cp -r /home/GeneValidator/* /home/genevalidator-2.1.10-linux-x86_64/ && "
        #"chmod -R uog+r /home/GeneValidator/genevalidator-2.1.10-linux-x86_64/lib/packages/mafft/mafftdir && "
        #"cp -r /genevalidator-2.1.10-linux-x86_64/* {input.geneValDir} && "
        #"chmod -R uog+rx {input.geneValDir} && "
        "mkdir -p GeneValDirs && mkdir -p {output.dirs} && "
        "(GeneValidator/bin/genevalidator --db {params.blastDb} --num_threads 1 {input.proteome} -o {output.dirs} > {output.summary}) > {log}"
