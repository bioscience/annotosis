
rule buscoAfter:
    input:
        proteome="{basename}/proteome.fa".format(basename=os.path.basename(config["workDir"])),
        selected="{basename}/scoresGeneSel.tsv".format(basename=os.path.basename(config["workDir"])),
    output:
        res=directory("buscoAfter"),
        good="{basename}/good.pts.fa".format(basename=os.path.basename(config["workDir"])),
        tmp=temp("{basename}/tmp.ids".format(basename=os.path.basename(config["workDir"]))),
    container:
        "docker://ezlabgva/busco:v5.1.2_cv1"
    params:
        config["lineage"]
    log:
        "logs/busco/buscoAfter.log"
    threads: workflow.cores
    shell:
        #"(busco -i /busco_wd/{output.good} -c {threads} -m prot -l {params} -r -o {output.res}) > {log} && "
        """sed 1,1d {input.selected} | sed 's/"//g' | awk '{{print $1}}' > {output.tmp} && """
        """awk -v ORS="" '/^>/{{$0="\\n"$0"\\n"}}1' {input.proteome} | sed 1,1d | fgrep -A 1 -f {output.tmp} > {output.good} && """ 
        "(busco -i {output.good} -c {threads} -m prot -l {params} -r -o {output.res}) > {log}"
