
rule prepareGeneVal:
    #input:
    #    #proteome=config["proteome"],
    output:
        dirs=directory("GeneValidator"),
    container:
        config["geneValidator"]
    shell:
        "rm -rf GeneValidator && mkdir -p GeneValidator && cp -P -r /genevalidator-2.1.10-linux-x86_64/* GeneValidator/ && chmod uog+x GeneValidator/bin/*"
