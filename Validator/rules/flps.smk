
rule flps:
    input:
        proteome="{basename}/proteome.fa".format(basename=os.path.basename(config["workDir"])),
        #proteome=config["proteome"],
        #proteome="FileDir/part{i}.fa",
    output:
        res="{basename}/flps.res".format(basename=os.path.basename(config["workDir"])),
        composition=temp("{basename}/composition.dat".format(basename=os.path.basename(config["workDir"])))
    container:
        config["flps"]
        #"docker://pakorhon/validator:latest"
    #params:
    #    blastDb=config["blastDb"]
    log:
        "logs/flps/flps.log"
    #threads: workflow.cores
    shell:
        #"cp -r /home/GeneValidator/* /home/genevalidator-2.1.10-linux-x86_64/ && "
        #"chmod -R uog+r /home/GeneValidator/genevalidator-2.1.10-linux-x86_64/lib/packages/mafft/mafftdir &&"
        #"mkdir -p GeneValDirs && mkdir -p {output.dirs} && "
        #"(genevalidator --db {params.blastDb} --num_threads 1 {input.proteome} -o {output.dirs} > {output.summary}) > {log}"
        "awk -f /fLPS/COMPOSITION.awk {input} > {output.composition} && "
        "/fLPS/bin/linux/fLPS -v -c {output.composition} {input} > {output.res} 2> {log}"