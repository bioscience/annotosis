#!/usr/bin/env python

import os, sys, argparse
from base import Base

#################################################
def options():
    parser = argparse.ArgumentParser('usage: python %prog -i filename -n size')
    parser.add_argument('-d', '--workdir', dest='workdir', help='Name of the working directory', metavar='WORKDIR', default='')
    parser.add_argument('-i', '--geneGff', dest='gene', help='Maker GFF file for genes', metavar="GENEGFF", default='')
    parser.add_argument('-r', '--repsGff', dest='reps', help='RepeatMasker GFF file', metavar="REPSGFF", default='')
    #parser.add_argument('-a', '--assemble', dest='assemble', action='store_true', help='Do assembly', default=False)
    arguments = parser.parse_args()
    if arguments.gene == '':
        parser.print_help()
        sys.exit(1)
    return arguments


#################################################
def main():
    '''
    '''
    opts = options()

    wd = "%s/Overlaps" %opts.workdir
    base = Base()
    base.createDir(wd)
    #memDir = os.getcwd()
    #os.chdir(wd)

    # Resolving repeat overlaps in exons
    base.shell("""sort -k 1,1 -k 4,4g %s | awk '{if ($3=="exon") print $0}' | bedtools intersect -wo -a stdin -b %s | awk '{print $1"\t"$4"\t"$5"\t"$9"\t"$13"\t"$14"\t"$NF}' | awk -F"=" '{print $1"\t"$3}' | sed 's/\tID//1' > %s/exonRepeat.ovl""" %(opts.gene, opts.reps, wd))
    base.shell("""awk '{print $4"\t"$5"\t"$6}' %s/exonRepeat.ovl | sort -k1,1 -k2,2g | bedtools merge -i stdin > %s/exonRepeat.merged""" %(wd, wd))
    base.shell("""awk 'BEGIN{while ((getline<"%s/exonRepeat.ovl")>0) h[$4]=h[$4]"###"$1"\t"$2"\t"$3}{split(h[$1],a,"###"); for (i=2;i<=length(a);i++) print $1"\t"a[i]"\trepeat\t"$2"\t"$3}' %s/exonRepeat.merged | uniq | sort -k1,1 -k3,3g > %s/exonRepeatMerged.ovl""" %(wd, wd, wd))
    base.shell("""awk '{print $2"\t"$3"\t"$4"\t"$1}' %s/exonRepeatMerged.ovl | uniq > %s/exon.coords""" %(wd, wd))
    base.shell("""awk '{print $2"\t"$6"\t"$7"\trepeat"}' %s/exonRepeatMerged.ovl | sort -k 1,1 -k 2,2g > %s/repeat.coords""" %(wd, wd))
    base.shell("""bedtools merge -i %s/repeat.coords > %s/repeat.coords.merged""" %(wd, wd))
    base.shell("""bedtools intersect -wo -a %s/exon.coords -b %s/repeat.coords.merged | sort | uniq > %s/exonRepeatOverlap.cnt""" %(wd, wd, wd))
    base.shell("""awk -F"\t" 'BEGIN{while((getline<"%s/exon.coords")>0) e[$4]+=$3-$2+1}{h[$4]+=$NF+1}END{for (i in h) print i"\t"h[i]"\t"e[i]"\t"h[i]/e[i]}' %s/exonRepeatOverlap.cnt > %s/exonRepeatOverlap.res""" %(wd, wd, wd))

    # Resolving repeat overlaps in CDSs
    base.shell("""sort -k 1,1 -k 4,4g %s | awk '{if ($3=="CDS") print $0}' | bedtools intersect -wo -a stdin -b %s | awk '{print $1"\t"$4"\t"$5"\t"$9"\t"$13"\t"$14"\t"$NF}' | awk -F"=" '{print $1"\t"$3}' | sed 's/\tID//1' > %s/cdsRepeat.ovl""" %(opts.gene, opts.reps, wd))
    base.shell("""awk '{print $4"\t"$5"\t"$6}' %s/cdsRepeat.ovl | sort -k1,1 -k2,2g | bedtools merge -i stdin > %s/cdsRepeat.merged""" %(wd, wd))
    base.shell("""awk 'BEGIN{while ((getline<"%s/cdsRepeat.ovl")>0) h[$4]=h[$4]"###"$1"\t"$2"\t"$3}{split(h[$1],a,"###"); for (i=2;i<=length(a);i++) print $1"\t"a[i]"\trepeat\t"$2"\t"$3}' %s/cdsRepeat.merged | uniq | sort -k1,1 -k3,3g > %s/cdsRepeatMerged.ovl""" %(wd, wd, wd))
    base.shell("""awk '{print $2"\t"$3"\t"$4"\t"$1}' %s/cdsRepeatMerged.ovl | uniq > %s/cds.coords""" %(wd, wd))
    base.shell("""awk '{print $2"\t"$6"\t"$7"\trepeat"}' %s/cdsRepeatMerged.ovl | sort -k 1,1 -k 2,2g > %s/repeat.coords""" %(wd, wd))
    base.shell("""bedtools merge -i %s/repeat.coords > %s/repeat.coords.merged""" %(wd, wd))
    base.shell("""bedtools intersect -wo -a %s/cds.coords -b %s/repeat.coords.merged | sort | uniq > %s/cdsRepeatOverlap.cnt""" %(wd, wd, wd))
    base.shell("""awk -F"\t" 'BEGIN{while((getline<"%s/cds.coords")>0) e[$4]+=$3-$2+1}{h[$4]+=$NF+1}END{for (i in h) print i"\t"h[i]"\t"e[i]"\t"h[i]/e[i]}' %s/cdsRepeatOverlap.cnt > %s/cdsRepeatOverlap.res""" %(wd, wd, wd))

    # Resolving repeat overlaps in genes containing introns
    base.shell("""sort -k 1,1 -k 4,4g %s | awk '{if ($3=="gene") print $0}' | bedtools intersect -wo -a stdin -b %s | awk '{print $1"\t"$4"\t"$5"\t"$9"\t"$13"\t"$14"\t"$NF}' | awk -F"=" '{print $1"\t"$2}' | sed 's/\tID//1' > %s/geneRepeat.ovl""" %(opts.gene, opts.reps, wd))
    base.shell("""awk '{print $4"\t"$5"\t"$6}' %s/geneRepeat.ovl | sort -k1,1 -k2,2g | bedtools merge -i stdin > %s/geneRepeat.merged""" %(wd, wd))
    base.shell("""awk 'BEGIN{while ((getline<"%s/geneRepeat.ovl")>0) h[$4]=h[$4]"###"$1"\t"$2"\t"$3}{split(h[$1],a,"###"); for (i=2;i<=length(a);i++) print $1"\t"a[i]"\trepeat\t"$2"\t"$3}' %s/geneRepeat.merged | uniq | sort -k1,1 -k3,3g > %s/geneRepeatMerged.ovl""" %(wd, wd, wd))
    base.shell("""awk '{print $2"\t"$3"\t"$4"\t"$1}' %s/geneRepeatMerged.ovl | uniq > %s/gene.coords""" %(wd, wd))
    base.shell("""awk '{print $2"\t"$6"\t"$7"\trepeat"}' %s/geneRepeatMerged.ovl | sort -k 1,1 -k 2,2g > %s/repeat.coords""" %(wd, wd))
    base.shell("""bedtools merge -i %s/repeat.coords > %s/repeat.coords.merged""" %(wd, wd))
    base.shell("""bedtools intersect -wo -a %s/gene.coords -b %s/repeat.coords.merged | sort | uniq > %s/geneRepeatOverlap.cnt""" %(wd, wd, wd))
    base.shell("""awk -F"\t" 'BEGIN{while((getline<"%s/gene.coords")>0) e[$4]+=$3-$2+1}{h[$4]=$NF+1}END{for (i in h) print i"\t"h[i]"\t"e[i]"\t"h[i]/e[i]}' %s/geneRepeatOverlap.cnt > %s/geneRepeatOverlap.res""" %(wd, wd, wd))
    #awk '{print $1"-mRNA-1\t"$2"\t"$3"\t"$4}' geneRepeatOverlap.res > geneRepeatOverlap.mrna.res

    # Resolving repeat overlaps in mRNAs containing introns
    #base.shell("""sort -k 1,1 -k 4,4g %s | awk '{if ($3=="mRNA") print $0}' | bedtools intersect -wo -a stdin -b %s | awk '{print $1"\t"$4"\t"$5"\t"$9"\t"$13"\t"$14"\t"$NF}' | awk -F["=;"] '{print $1,"\t"$2"\t"$12}' | awk -F"\t" '{print $1"\t"$2"\t"$3"\t"$5"\t"$7"\t"$8"\t"$9}' > %s/mRNARepeat.ovl""" %(opts.gene, opts.reps, wd))
    base.shell("""sort -k 1,1 -k 4,4g %s | awk '{if ($3=="mRNA") print $0}' | bedtools intersect -wo -a stdin -b %s | awk '{print $1"\t"$4"\t"$5"\t"$9"\t"$13"\t"$14"\t"$NF}' | awk -F["=;\t"] '{print $1"\t"$2"\t"$3"\t"$5"\t"$8"\t"$9"\t"$10}' > %s/mRNARepeat.ovl""" %(opts.gene, opts.reps, wd))
    base.shell("""awk '{print $4"\t"$5"\t"$6}' %s/mRNARepeat.ovl | sort -k1,1 -k2,2g | bedtools merge -i stdin > %s/mRNARepeat.merged""" %(wd, wd))
    base.shell("""awk 'BEGIN{while ((getline<"%s/mRNARepeat.ovl")>0) h[$4]=h[$4]"###"$1"\t"$2"\t"$3}{split(h[$1],a,"###"); for (i=2;i<=length(a);i++) print $1"\t"a[i]"\trepeat\t"$2"\t"$3}' %s/mRNARepeat.merged | uniq | sort -k1,1 -k3,3g > %s/mRNARepeatMerged.ovl""" %(wd, wd, wd))
    base.shell("""awk '{print $2"\t"$3"\t"$4"\t"$1}' %s/mRNARepeatMerged.ovl | uniq > %s/mRNA.coords""" %(wd, wd))
    base.shell("""awk '{print $2"\t"$6"\t"$7"\trepeat"}' %s/mRNARepeatMerged.ovl | sort -k 1,1 -k 2,2g > %s/repeat.coords""" %(wd, wd))
    base.shell("""bedtools merge -i %s/repeat.coords > %s/repeat.coords.merged""" %(wd, wd))
    base.shell("""bedtools intersect -wo -a %s/mRNA.coords -b %s/repeat.coords.merged | sort | uniq > %s/mRNARepeatOverlap.cnt""" %(wd, wd, wd))
    base.shell("""awk -F"\t" 'BEGIN{while((getline<"%s/mRNA.coords")>0) e[$4]+=$3-$2+1}{h[$4]=$NF+1}END{for (i in h) print i"\t"h[i]"\t"e[i]"\t"h[i]/e[i]}' %s/mRNARepeatOverlap.cnt > %s/mRNARepeatOverlap.res""" %(wd, wd, wd))
 
    #base.shell("""python ~/Codebase/joiner.py -i "%s/mRNARepeatOverlap.res %s/exonRepeatOverlap.res %s/cdsRepeatOverlap.res" > %s/joined.res""" %(wd, wd, wd, wd))

    #with open("%s/joined.res" %wd) as handle:
    #    for line in handle:
    #        print (line.strip())

#################################################
if __name__ == "__main__":
    main()

