import os, sys, optparse
#import concurrent.futures
#from itertools import repeat
#from multiprocessing import Process, Manager, Queue, Lock
#import rpy2.robjects as robjects
#from rpy2.robjects import pandas2ri
import numpy as np
#import statistics as stats
from scipy.sparse import csr_matrix
from sklearn.cluster import AgglomerativeClustering
import scipy.cluster.hierarchy as shc
import matplotlib.pyplot as plt
import fastcluster
#from scipy.sparse import lil_matrix
#from scipy.io import mmwrite
sys.setrecursionlimit(1000000)

#################################################
def options():
    parser = optparse.OptionParser('usage: python %prog -i filename -n size')
    parser.add_option('-d', '--wd', dest='wd', help='Work directory', metavar='WORKDIR', default='')
    parser.add_option('-i', '--clusters', dest='clusters', help='Bootstrapped kmeans clusters', metavar='CLUSTERS', default='')
    #parser.add_option('-c', '--centers', dest='centers', help='Centers of bootstrapped kmeans clusters', metavar='CENTERS', default='')
    #parser.add_option('-s', '--sizes', dest='sizes', help='Sizes of bootstrapped kmeans clusters', metavar='SIZES', default='')
    #parser.add_option('-T', '--threads', dest='threads', help='Number of parallel threads (default 24)', metavar='THREADS', default='24')
    #parser.add_option('-r', '--order', dest='order', action='store_true', help='Longest first (default=FALSE)', default=False)
    #parser.add_option('-a', '--assemble', dest='assemble', action='store_true', help='Do assembly', default=False)
    options, args = parser.parse_args()
    if options.clusters == '':
        parser.print_help()
        sys.exit(1)
    return options

#################################################
def main():
    '''
    '''
    opts = options()

    clusters = opts.clusters.strip('"').strip("'").split()
    #centers = opts.centers.strip('"').strip("'").split()
    #sizes = opts.sizes.strip('"').strip("'").split()

    # Link gene identifiers to indices
    bootCnt, cIds, gIds = len(clusters), set(), []
    with open(clusters[0]) as handle:
        for line in handle:
            items = line.strip().split()
            if len(items) < 2: continue
            gId, cId = items[0].strip('"'), items[1]
            cIds.add(cId)
            gIds.append(gId)
    clusterCnt = len(cIds)
    gIds = sorted(gIds)
    dGeneIdxs = {}
    with open("%s/gene.idxs" %opts.wd, 'w') as handle:
        for i in range(len(gIds)):
            dGeneIdxs[gIds[i]] = i
            handle.write("%s\t%d\n" %(gIds[i], i))

    mat = csr_matrix((len(gIds), len(gIds)), dtype=np.double).todense()
    for i in range(bootCnt): # Increment cells by one for genes in same clusters
        print ("### Round %d of %d" %(i + 1, bootCnt))
        with open(clusters[i]) as handle:
            dClusters = {}
            for line in handle:
                items = line.strip().split()
                if len(items) < 2: continue
                gId, cId = items[0].strip('"'), items[1]
                try:
                    dClusters[cId].append(gId)
                except KeyError:
                    dClusters[cId] = []
                    dClusters[cId].append(gId)
            for cId in dClusters:
                gIdxs = sorted([dGeneIdxs[gId] for gId in dClusters[cId]])
                for gIdx in gIdxs: # could be improved by slicing consecutive idxs if present
                  mat[gIdx, gIdxs] += 1

    for i in range(len(gIds)):
        mat[i, i] = float(bootCnt)
    mat = 1.0 - mat / float(bootCnt)
    np.save("%s/mat.npy" %opts.wd, mat)

    #print ("### Clustering ...")
    #Z = fastcluster.linkage(mat.A, method ='ward')
    ##cluster = AgglomerativeClustering(n_clusters = clusterCnt, affinity='euclidean', linkage='ward')
    ##cluster.fit_predict(mat.A)
    ##print (cluster.labels_)
    #print ("### Clustering done ...")
    ##with open("%s/clusters.txt" %opts.wd) as handle:
    ##    for i in range(len(cluster.labels_)):
    ##        handle.write("%s\t%d" %(gIds[i], cluster.labels_[i]))
    #
    ##ac2 = AgglomerativeClustering(n_clusters = 2)
    #
    ## Visualizing the clustering
    #plt.figure(figsize =(6, 6))
    #plt.scatter(X_principal['P1'], X_principal['P2'], c = ac2.fit_predict(X_principal), cmap ='rainbow')
    #
    ## plot the dendrogram
    #threshold = Z[-clusterCnt + 1, 2]
    #dg = shc.dendrogram(Z, no_labels = True, color_threshold = threshold)
    ## plot color bars under the leaves
    ##color = [classes[k] for k in dg['leaves']]
    ##b = .1 * Z[-1, 2]
    ##plt.bar(np.arange(N) * 10, np.ones(N) * b, bottom = -b, width = 10,
    ##        color = color, edgecolor = 'none')
    ##plt.gca().set_ylim((-b, None))
    #plt.savefig("clusters.png")
    #
    #plt.figure(figsize =(8, 8))
    #plt.title('Visualising the data')
    #Dendrogram = shc.dendrogram(cluster)
    #plt.savefig('clusters.png')
    
#################################################
if __name__ == "__main__":
    main()
