#!/usr/bin/env python
import os, sys, optparse

#################################################
def options():
    parser = optparse.OptionParser('usage: python %prog -i "masked1.fa masked2.fa ... maskedN.fa"')
    parser.add_option('-i', '--fasta', dest='fasta', help='FASTA file to be divided for InterPro.', metavar='FASTA', default='')
    parser.add_option('-n', '--batchCnt', dest='batchCnt', help='Sequence cound per file.', metavar='SEQCNT', default='1000')
    options, args = parser.parse_args()
    if options.fasta == '':
        parser.print_help()
        sys.exit(1)
    return options


#################################################
def main():
    '''
    '''
    opts = options()
    batchCnt = int(opts.batchCnt)
    fileCnt, seqCnt = 0, 0
    bundle = []
    with open(opts.fasta) as handle:
        handleW = open("part%d.fa" %(fileCnt + 1), 'w')
        for line in handle:
            if line[0] == ">":
                seqCnt += 1
                if seqCnt % batchCnt == 0:
                    for item in bundle:
                        handleW.write("%s" %item)
                    handleW.close()
                    fileCnt += 1
                    handleW = open("part%d.fa" %(fileCnt + 1), 'w')
                    bundle = []
            bundle.append(line)
        for item in bundle:
            handleW.write("%s" %item)
        handleW.close()
            

#################################################
if __name__ == "__main__":
    main()
