#!/usr/bin/env python

import os, sys, optparse

#################################################
def options():
    parser = optparse.OptionParser('usage: python %prog -i "file1 file2 ... fileN"')
    parser.add_option('-i', '--files', dest='files', help='Joins files together using first column as identifiers', metavar='FILES', default='-')
    parser.add_option('-e', '--empty', dest='empty', help='The value to use for empty cells. Default is 0', metavar='EMPTY', default='0')
    #parser.add_option('-a', '--assemble', dest='assemble', action='store_true', help='Do assembly', default=False)
    options, args = parser.parse_args()
    if options.files == '':
        parser.print_help()
        sys.exit(1)
    return options


#################################################
def main():
    '''
    '''
    opts = options()
    dFinal = {}
    files = opts.files.strip('"').strip("'").split()
    for name in files:
        handle = open(name, 'r')
        for line in handle:
            items = line.strip().split('\t')
            dFinal[items[0]] = []
        handle.close()

    maxLen = 0
    for name in files:
        with open(name, 'r') as handle:
            for line in handle:
                items = line.strip().split('\t')
                try:
                    for i in range(1, len(items)):
                        dFinal[items[0]].append(items[i])
                except KeyError:
                    dFinal[items[0]] = []
                    for i in range(1, len(items)):
                        dFinal[items[0]].append(items[i])
                if len(dFinal[items[0]]) > maxLen: maxLen = len(dFinal[items[0]])
        for key in dFinal:
            if len(dFinal[key]) < maxLen:
                for i in range(maxLen - len(dFinal[key])):
                    dFinal[key].append(opts.empty)
    for key in dFinal:
        print ("%s\t%s" %(key, '\t'.join(dFinal[key])))
    #for key in dFinal:
    #    myRow = "%s" %key
    #    for item in dFinal[key]:
    #        myRow += "\t%d" %int(float(item))
    #    print myRow


#################################################
if __name__ == "__main__":
    main()
