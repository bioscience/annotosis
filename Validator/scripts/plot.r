library(tidyverse)
library(RColorBrewer)
library(devtools)
library(Rtsne)
library(fpc)
library(pastecs)
library(zoo)

args = commandArgs(trailingOnly=TRUE)
#clusterData <- args[1]
dataFile <- args[1]
baseName <- args[2]
clusterCnt <- args[3]
smoothing <- as.integer(args[4])
#clusters <- read.table(clusterData, header=FALSE, row.names=1)
##clusters <- clusters[, ncol(clusters)]

data <- read.table(dataFile, header=TRUE, row.names=1)
#data[, "EVIDENCE"] <- 100.0 - data[,"EVIDENCE"]
#data[, "EXONCNT"] <- -data[,"EXONCNT"]
#summary(data)
data <- scale(data)
scaleC <- attr(data, 'scaled:scale')
centerS <- attr(data, 'scaled:center')
#dataColNames <- colnames(data)
#data <- data[rownames(clusters),]

cf <- clusterboot(data, B=100, bootmethod="boot", clustermethod=kmeansCBI, krange=8, seed=1)
#cf <- clusterboot(data, B=100, bootmethod="boot", clustermethod=kmeansCBI, krange=8, seed=32980)
#print (cf)
#str(cf)
#str(cf$result$result$cluster)
jpeg("WorkDir/box.jpg", width=1200, height=1000)
boxplot(t(cf$bootresult))
dev.off()
write.table(cf$result$result$cluster, "WorkDir/bootClusters.tsv", append = FALSE, sep = "\t", row.names = TRUE, col.names = FALSE)
#
d <- prcomp(data)
jpeg(paste(baseName, "Biplot.jpg", sep=""), width=1200, height=800)
biplot(d, xlabs=rep(".", nrow(data)))
dev.off()

## Create elbow plot
#kmean_withinss <- function(k) {
#  cluster <- kmeans(data, k)
#  return (cluster$tot.withinss)
#}
#maxK <- 20 
#wss <- sapply(2:maxK, kmean_withinss)
#elbow <- data.frame(2:maxK, wss)
#jpeg(paste(baseName, "Elbow.jpg", sep=""), width=1200, height=1000)
#ggplot(elbow, aes(x = X2.maxK, y = wss)) + geom_point() + geom_line() + scale_x_continuous(breaks = seq(1, 20, by = 1))
#dev.off()
#
##dataMod <- cbind(data, clusters[,ncol(clusters)])
##colnames(dataMod) <- c(dataColNames, "CLUSTER")
##centers <- NULL
##for (i in 1:clusterCnt) {
##centers <- rbind(centers, colMeans(dataMod[dataMod[,"CLUSTER"] == i,]))
##}
#
centers <- cf$result$result$centers
cluster <- 1:clusterCnt
center_df <- data.frame(cluster, centers)
center_reshape <- gather(center_df, features, values, EXONCNT:GVS)

# Create the palette
hm.palette <- colorRampPalette(brewer.pal(10, 'RdYlGn'), space='Lab')

# Plot the heat map
jpeg(paste(baseName, "Heatmap.jpg", sep=""), width=1200, height=1000)
ggplot(data = center_reshape, aes(x = features, y = cluster, fill = values)) +
  scale_y_continuous(breaks = seq(1, clusterCnt, by = 1)) +
  geom_tile() + coord_equal() + scale_fill_gradientn(colours = hm.palette(90)) + theme_classic()
dev.off()

#cls$centers
#print (centers[,-ncol(centers)])
#print (t(as.matrix(centers[,-ncol(centers)])))
print (t(as.matrix(centers)))
#print (table(dataMod[,"CLUSTER"]))
print (cf$result$result$size)
#print (attr(data, 'scaled:scale'))
#print (attr(data, 'scaled:center'))
#print (scaleC)
#print (centerS)
#print (t(as.matrix(centers[,-ncol(centers)])) * scaleC + centerS)
print (t(as.matrix(centers)) * scaleC + centerS)
#cls$size

# Confirm that bad predictions stay negative
if (d$rotation["GPCT", "PC1"] > 0) { d$rotation[,1] <- -d$rotation[,1] }

scores <- centers %*% d$rotation[,1]
#scores <- NULL
#scores <- cbind(scores, 0.1 * centers[,"EXONCNT"])
#scores <- cbind(scores, 0.15 * centers[, "EVIDENCE"])
#scores <- cbind(scores, 0.1 * centers[, "CDSLEN"])
#scores <- cbind(scores, -1.0 * centers[, "ANNO"])
#scores <- cbind(scores, -0.3 * centers[, "GPCT"])
#scores <- cbind(scores, -0.9 * centers[, "EPCT"])
#scores <- cbind(scores, -0.0 * centers[, "CPCT"])
#scores <- cbind(scores, -0.3 * centers[, "GRPLEN"])
#scores <- cbind(scores, 1.0 * centers[, "FLPS"])
print (rowSums(scores))

scores <- data %*% d$rotation[,1]
#scores <- NULL
#scores <- cbind(scores, 0.2 * data[,"EXONCNT"])
#scores <- cbind(scores, 0.25 * data[, "EVIDENCE"])
#scores <- cbind(scores, 0.2 * data[, "CDSLEN"])
#scores <- cbind(scores, -1.0 * data[, "ANNO"])
#scores <- cbind(scores, -0.8 * data[, "GPCT"])
#scores <- cbind(scores, -0.9 * data[, "EPCT"])
#scores <- cbind(scores, -0.0 * data[, "CPCT"])
#scores <- cbind(scores, -0.3 * data[, "GRPLEN"])
#scores <- cbind(scores, -0.4 * data[, "FLPS"])
write.table(rowSums(scores), paste(baseName, "Gene.tsv", sep=""), append = FALSE, sep = "\t", row.names = TRUE, col.names = TRUE)
#head(rowSums(scores))

geneScores <- sort(rowSums(scores))
geneClusters <- cf$result$result$cluster[names(geneScores)]
#geneClusters <- clusters[names(geneScores), ncol(clusters)]
geneScores <- cbind(geneScores, geneClusters)
write.table(geneScores, "WorkDir/geneScoresWithClusters.tsv", append = FALSE, sep = "\t", row.names = TRUE, col.names = TRUE)

x <- 1:nrow(data)
y <- geneScores[,1]
logEstimate <- lm(y ~ log(x))
#polEstimate <- lm(y ~ x + I(x^2) + I(x^3) + I(x^4) + I(x^5))
print (paste(length(x), length(y)))
splEstimate <- smooth.spline(x, y, df=smoothing)
str(splEstimate)
print (smoothing)

x <- splEstimate$x
y <- splEstimate$y
# Find the proposed shoulder cutoff
d1 <- NULL
for (i in 1:(length(x) - 1) ) {
  d1 <- c(d1, y[i + 1] - y[i])
}

d2 <- NULL
for (i in 1:(length(d1) - 1) ) {
  d2 <- c(d2, d1[i + 1] - d1[i])
}

jpeg("d1.jpg", width=1200, height=1000)
plot(1:length(d1), d1, type="l", col="red", xlim=c(1,2000))
#lines(1:length(d2), d2, type="l", col="blue")
dev.off()
jpeg("d2.jpg", width=1200, height=1000)
plot(1:length(d2), d2, type="l", col="red", xlim=c(1,2000))
#lines(1:length(d2), d2, type="l", col="blue")
dev.off()

findPeaks <- function (x, m = 3){
    shape <- diff(sign(diff(x, na.pad = FALSE)))
    pks <- sapply(which(shape < 0), FUN = function(i){
       z <- i - m + 1
       z <- ifelse(z > 0, z, 1)
       w <- i + m + 1
       w <- ifelse(w < length(x), w, length(x))
       if(all(x[c(z : i, (i + 2) : w)] <= x[i + 1])) return(i + 1) else return(numeric(0))
    })
    pks <- unlist(pks)
    pks
}

print (findPeaks(-d2, m = 5000))
#pits <- findPeaks(-d2, m = 5000)
pit <- which(min(d2) == d2)

write.table(geneScores[pit:nrow(geneScores), 1], paste(baseName, "GeneSel.tsv", sep=""), append = FALSE, sep = "\t", row.names = TRUE, col.names = TRUE)

jpeg(paste(baseName, "Gene.jpg", sep=""), width=1200, height=1000)
#plot(1:nrow(data), geneScores, type="l")
plot(1:nrow(data), geneScores[,1], type="l", ylim=c(min(scores) - 1, max(max(scores), max(geneScores[,2])) + 1))
points(1:nrow(data), jitter(geneScores[,2], 2), col=geneScores[,2], pch=20) 
#abline(a=-maxI, b=1, col="red")
#lines(1:nrow(data), predict(logEstimate), col="green")
lines(splEstimate, col="red")
abline(v=pit, col="blue")
dev.off()

##jpeg(paste(baseName, "Scatter.jpg", sep=""), width=1200, height=1000)
##plot(pca$x[,1], pca$x[,2], col=c("red","blue","green","orange","black","yellow","brown","pink")[clusters])
##dev.off()

