#!/usr/bin/env python

import os, sys, optparse, getpass
from multiprocessing import Process, Pipe
from base import Base
from fasta import Fasta

#################################################
def options():
    parser = optparse.OptionParser('usage: %prog -i "proteins1.fa proteins2.fa ... proteinsN.fa" -l "lab1 lab2 ... labN" -p "1 3 ... 1" -e 1e-5 -s 0.6')
    parser.add_option('-d', '--workdir', dest='workdir', help='Name of the working directory', metavar='WORKDIR', default='')
    parser.add_option('-i', '--filenames', dest='filenames', help='Names of the files of species containing the proteins', metavar='FILES', default='')
    parser.add_option('-l', '--labels', dest='labs', help="Labels for each species", metavar='LABELS', default='')
    parser.add_option('-p', '--positions', dest='positions', help="Default positions of unique identifier in FASTA header separated by |. Default position is 1 for all.", metavar='POSITIONS', default='')
    parser.add_option('-e', '--evalue', dest='evalue', help="E-value used at blast. Default is 1e-5. Use 1e-X format only!", metavar='EVALUE', default='1e-5')
    parser.add_option('-s', '--similarity', dest='sim', help="Required similarity (0 .. 1). Default if 0.5", metavar='SIM', default='0.5')
    parser.add_option('-m', '--minlen', dest='minlen', help="Allowed minimum lenght of a protein. Default is 20.", metavar='MINLEN', default='20')
    parser.add_option('-b', '--noblast', dest='skipBlast', action='store_true', help="Skip BLAST", default=False)
    parser.add_option('-T', '--threads', dest='threads', help="Number of threads for BLAST.", metavar='THREADS', default='1')
    parser.add_option('-S', '--socket', dest='socket', help="Socket path to mysql server", metavar='SOCKET', default='')
    #parser.add_option('-a', '--add', dest='add', action='store_true', help="Adds given species and labels to existing BLAST", default=False)
    #parser.add_option('-u', '--uname', dest='uname', help='Username', metavar='Unix username at the remove server.', default='')
    options, args = parser.parse_args()
    if options.filenames == '' or options.labs == '':
        parser.print_help()
        print ('\nE.g.: orthoMcl -i "proteome1.fa proteome2.fa" -l "Tax Tvi" -p "4 4" -e 1e-5 -s 0.5')
        print ("Results will be found in TmpOrthoMcl directory in groups.txt file.")
        print ("Note! The labels must be exactly 3 characters long and preferrably start with an upper case character.")
        sys.exit(1)
    return options


#################################################
def checkResidue(fastaFile):
    '''
    '''
    retVal = "nucleotides"
    try:
        limit = 100
        fasta = Fasta(fastaFile)
        for i in range(len(fasta.headers)):
            if i > limit: break
            seq = fasta.seqs[i].upper()
            for item in seq:
                if item not in ['A', 'T', 'C', 'G', 'N']:
                    retVal = "amino acids"
                    break
    except IOError:
        print ("Fatal error: file %s not found. Exiting..." %fastaFile)
        sys.exit()
    return retVal


#################################################
def checkUniqueIds(fastaFile):
    '''
    '''
    fasta = Fasta(fastaFile)
    if len(fasta.headers) != len(set(fasta.headers)):
        print ("Fatal error: FASTA sequence identifiers are not unique in %s. Exiting..." %fastaFile)
        print ("Probably position for this file is given wrong...")
        print ("%d %d" %(len(fasta.headers), len(set(fasta.headers))))
        print (fasta.headers)
        sys.exit()


#################################################
def createOrthoMclConfigFile(wd, userName, eValue, similarity, socket):
    '''
    '''
    eValue = eValue.split('e')[1]
    similarity = int(float(similarity) * 100.0)
    handle = open("%s/orthomcl.config" %wd, 'w')
    handle.write("# this config assumes a mysql database named 'orthomcl'.  adjust according\n")
    handle.write("# to your situation.\n")
    handle.write("dbVendor=mysql\n")
    handle.write("dbConnectString=dbi:mysql:ortho%s:mysql_local_infile=1;mysql_socket=%s\n" %(userName, socket))
    handle.write("dbLogin=ortho%s\n" %userName)
    handle.write("dbPassword=password\n")
    handle.write("similarSequencesTable=SimilarSequences\n")
    handle.write("orthologTable=Ortholog\n")
    handle.write("inParalogTable=InParalog\n")
    handle.write("coOrthologTable=CoOrtholog\n")
    handle.write("interTaxonMatchView=InterTaxonMatch\n")
    handle.write("percentMatchCutoff=%d\n" %similarity)
    handle.write("evalueExponentCutoff=%s\n" %eValue)
    handle.write("oracleIndexTblSpc=NONE\n")
    handle.close()


#################################################
def createMySqlScripts(wd, userName):
    '''
    '''
    handle = open("%s/createDb.sql" %wd, 'w')
    handle.write("set global local_infile=1;\n")
    handle.write("create user 'ortho%s'@'localhost' identified by 'password';\n" %userName)
    handle.write("create database ortho%s;\n" %userName)
    handle.write("grant all on ortho%s.* TO 'ortho%s'@'localhost';\n" %(userName, userName))
    handle.close()
    handle = open("%s/dropDb.sql" %wd, 'w')
    handle.write("drop database if exists ortho%s;\n" %userName)
    handle.write("drop user if exists 'ortho%s'@'localhost';\n" %userName)
    handle.close()

#################################################
def callShell(base, cmdStr, dummy = None):
    '''
    '''
    base.shell(cmdStr)

#################################################
def main():
    '''
    '''
    opts = options() # files contains exactly two PE files

    socket = opts.socket
    pCnt = int(opts.threads)
    eValue = opts.evalue
    similarity = opts.sim
    minlen = opts.minlen
    files = opts.filenames.split()
    labels = opts.labs.split()
    if len(labels) != len(set(labels)):
        print ("Fatal error: duplicate labels found. Exiting...")
        sys.exit(0)
    if len(files) != len(set(files)):
        print ("Fatal error: duplicate fasta file names found. Exiting...")
        sys.exit(0)
    positions = None
    if opts.positions != "":
        positions = opts.positions.split()
    if positions == None:
        positions = []
        for i in range(len(files)):
            positions.append("1")
    if len(files) != len(labels):
        print ("Fatal error: number of files does not match with the number of labels. Exiting...")
        sys.exit(0)
    if len(positions) != len(labels):
        print ("Fatal error: number of labels does not match with the number of positions of the ids. Exiting...")
        sys.exit(0)
    for lab in labels:
        if len(lab) != 3:
            print ("Fatal error: labels have to be exactly three characters long. Exiting...")
            sys.exit(0)

    base = Base()
    wd = "%s/OrthoMcl" %opts.workdir
    wdFasta = "%s/Fasta" %wd
    wdAdds = "%s/Adds" %wd
    base.createDir(wd)
    logHandle = open("%s/log.txt" %wd, 'w')
    base.setHandle(logHandle)
    base.createDir(wdFasta)
    userName = getpass.getuser()
    createOrthoMclConfigFile(wd, userName, eValue, similarity, socket)
    createMySqlScripts(wd, userName)

    requiredMolType = "amino acids"
    for myFile in files:
        molType = checkResidue(myFile)
        if requiredMolType != molType:
            print ("Fatal error: files have to all be amino acids. Exiting...")
            print ("File %s failed and was %s." %(myFile, molType))
            sys.exit(0)

    base.shell("rm -f %s/*.fasta" %wd)
    base.shell("rm -f %s/*.fasta" %wdFasta)
    for i in range(len(files)):
        myLab, myFile, myPos = labels[i], files[i], positions[i]
        if myFile == "%s.fasta" %myLab:
            print ("Fatal error: orthoMCL produces same filenames what you already have. Please renate your fasta files to .fa instead of .fasta. Exiting...")
            sys.exit(0)
        base.shell("orthomclAdjustFasta %s %s %s" %(myLab, myFile, myPos))
        checkUniqueIds("%s.fasta" %myLab)
        base.shell("mv -f %s.fasta %s" %(myLab, wdFasta))

    if opts.skipBlast == False:
        base.shell("orthomclFilterFasta %s %s 20" %(wdFasta, minlen))
        base.shell("mv -f poorProteins.* %s" %wd)

    # Blast all against all
    if opts.skipBlast == False:
        base.shell("makeblastdb -in goodProteins.fasta -dbtype prot")
    blastEvalue = eValue
    if float(blastEvalue) < 1e-5: blastEvalue = "1e-5"
    if opts.skipBlast == False: # and opts.add == False:
        base.shell("blastp -db goodProteins.fasta -query goodProteins.fasta -outfmt 6 -evalue %s -num_threads %d > goodProteins.blast" %(blastEvalue, pCnt))
    base.shell("""awk '{if ($11<=%s) print $0}' goodProteins.blast | grep -v "^#" > %s/filtered.blast""" %(eValue, wd))
    base.shell("mv -f goodProteins.* %s" %wd)

    base.shell("orthomclBlastParser %s/filtered.blast %s > %s/similarSequences.txt" %(wd, wdFasta, wd))
    # Prepare database
    base.shell("mysql -S %s --user=root --password=password < %s/dropDb.sql" %(socket, wd))
    base.shell("mysql -S %s --user=root --password=password < %s/createDb.sql" %(socket, wd))
    base.shell("orthomclInstallSchema %s/orthomcl.config" %wd)
    base.shell("orthomclLoadBlast %s/orthomcl.config %s/similarSequences.txt" %(wd, wd))
    # Identify potential orthologs
    base.shell("orthomclPairs %s/orthomcl.config %s/orthomclPairs.log cleanup=no" %(wd, wd))
    base.shell("rm -rf pairs")
    base.shell("rm -rf %s/pairs" %wd)
    base.shell("orthomclDumpPairsFiles %s/orthomcl.config" %wd)
    base.shell("mv -f pairs %s" %wd)
    # Group the orthologs
    base.shell("mcl mclInput --abc -I 2.0 -o mclOutput")
    base.shell("orthomclMclToGroups OWN_ 1 < mclOutput > %s/groups.txt" %wd)
    base.shell("mv -f mclInput %s" %wd)
    base.shell("mv -f mclOutput %s" %wd) 
    logHandle.close()

if __name__ == "__main__":
    main()
