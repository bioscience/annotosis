import os, sys, optparse
#import concurrent.futures
#from itertools import repeat
#from multiprocessing import Process, Manager, Queue, Lock
#import rpy2.robjects as robjects
#from rpy2.robjects import pandas2ri
import numpy as np
#import statistics as stats
from scipy.sparse import csr_matrix
#from sklearn.cluster import AgglomerativeClustering
from scipy.spatial.distance import pdist, squareform
from scipy.cluster.hierarchy import dendrogram
from scipy.cluster.hierarchy import fcluster
import matplotlib.pyplot as plt
import fastcluster
#from scipy.sparse import lil_matrix
#from scipy.io import mmwrite
#sys.setrecursionlimit(1000000)

#################################################
def options():
    parser = optparse.OptionParser('usage: python %prog -i filename -n size')
    parser.add_option('-d', '--wd', dest='wd', help='Work directory', metavar='WORKDIR', default='')
    parser.add_option('-i', '--idxs', dest='idxs', help='Indices for gene identifiers in square matrix', metavar='IDXS', default='')
    parser.add_option('-m', '--matrix', dest='matrix', help='Bootstrapped matrix', metavar='MATRIX', default='')
    parser.add_option('-c', '--clusters', dest='clusters', help='Cluster assingment per gene', metavar='CLUSTERS', default='0')
    #parser.add_option('-T', '--threads', dest='threads', help='Number of parallel threads (default 24)', metavar='THREADS', default='24')
    #parser.add_option('-c', '--centers', dest='centers', help='Centers of bootstrapped kmeans clusters', metavar='CENTERS', default='')
    #parser.add_option('-s', '--sizes', dest='sizes', help='Sizes of bootstrapped kmeans clusters', metavar='SIZES', default='')
    #parser.add_option('-r', '--order', dest='order', action='store_true', help='Longest first (default=FALSE)', default=False)
    #parser.add_option('-a', '--assemble', dest='assemble', action='store_true', help='Do assembly', default=False)
    options, args = parser.parse_args()
    if options.matrix == '':
        parser.print_help()
        sys.exit(1)
    return options

#################################################
def main():
    '''
    '''
    opts = options()

    dClusters = {}
    with open(opts.clusters) as handle:
        for line in handle:
            items = line.strip().split('\t')
            gId, cId = items[0].strip('"'), int(items[1])
            try:
                dClusters[cId].append(gId)
            except KeyError:
                dClusters[cId] = []
                dClusters[cId].append(gId)

    dGeneIdxs = {}
    with open(opts.idxs) as handle:
        for line in handle:
            items = line.strip().split('\t')
            gId, idx = items[0], items[1]
            dGeneIdxs[gId] = int(idx)
    mat = np.load(opts.matrix)

    dConfidence = {}
    for cId in sorted(dClusters.keys()):
        s = 0.0
        for gId1 in dClusters[cId]:
            idx1 = dGeneIdxs[gId1]
            for gId2 in dClusters[cId]:
                idx2 = dGeneIdxs[gId2]
                s += mat[idx1, idx2]
        dConfidence["%d" %cId] = 1.0 - s / float(len(dClusters[cId])) / float(len(dClusters[cId]))

    with open("%s/confidence.txt" %opts.wd, 'w') as handle:
        for cId in dConfidence:
            handle.write("%s\t%e\n" %(cId, dConfidence[cId]))

    
#################################################
if __name__ == "__main__":
    main()
