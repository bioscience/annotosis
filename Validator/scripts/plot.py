import os, sys, optparse
import csv
#import concurrent.futures
#from itertools import repeat
#from multiprocessing import Process, Manager, Queue, Lock
#import rpy2.robjects as robjects
#from rpy2.robjects import pandas2ri
import numpy as np
#import statistics as stats
from scipy.sparse import csr_matrix
#from sklearn.cluster import AgglomerativeClustering
from scipy.spatial.distance import pdist, squareform
from scipy.cluster.hierarchy import dendrogram
from scipy.cluster.hierarchy import fcluster
import matplotlib.pyplot as plt
import fastcluster
from sklearn.decomposition import TruncatedSVD
#from scipy.sparse import lil_matrix
#from scipy.io import mmwrite
sys.setrecursionlimit(1000000)

#################################################
def options():
    parser = optparse.OptionParser('usage: python %prog -i filename -n size')
    parser.add_option('-d', '--wd', dest='wd', help='Work directory', metavar='WORKDIR', default='')
    parser.add_option('-i', '--idxs', dest='idxs', help='Indices for gene identifiers in square matrix', metavar='IDXS', default='')
    parser.add_option('-m', '--data', dest='data', help='Data matrix', metavar='DATA', default='')
    #parser.add_option('-m', '--matrix', dest='matrix', help='Bootstrapped matrix', metavar='MATRIX', default='')
    parser.add_option('-c', '--cnt', dest='cnt', help='Number of expected clusters', metavar='CNT', default='0')
    #parser.add_option('-T', '--threads', dest='threads', help='Number of parallel threads (default 24)', metavar='THREADS', default='24')
    #parser.add_option('-c', '--centers', dest='centers', help='Centers of bootstrapped kmeans clusters', metavar='CENTERS', default='')
    #parser.add_option('-s', '--sizes', dest='sizes', help='Sizes of bootstrapped kmeans clusters', metavar='SIZES', default='')
    #parser.add_option('-r', '--order', dest='order', action='store_true', help='Longest first (default=FALSE)', default=False)
    #parser.add_option('-a', '--assemble', dest='assemble', action='store_true', help='Do assembly', default=False)
    options, args = parser.parse_args()
    if options.matrix == '':
        parser.print_help()
        sys.exit(1)
    return options

#################################################
def main():
    '''
    '''
    opts = options()
    clusterCnt = float(opts.cnt)

    dGeneIdxs = {}
    clusters = []
    with open(opts.idxs) as handle:
        for line in handle:
            items = line.strip().split('\t')
            gId, idx, cId = items[0], items[1], items[3]
            dGeneIdxs[gId] = cId, idx
            clusters.append(float(cId) / clusterCnt)
    mat = np.load(opts.matrix)

    mat = csv.reader(opts.matrix, delimiter='\t')

    #reduced = PCA(n_components=n_features)
    reduced = TruncatedSVD(n_components=2)
    reduced.fit(mat)
    X = reduced.transform(mat)
    #logging.debug("Reduced number of features to {0}".format(n_features))
    #logging.debug("Percentage explained: %s\n" % reducer.explained_variance_ratio_.sum())

    plt.figure(figsize=(12, 12))
    plt.scatter(X[:,0], X[:,1], c=clusters, cmap='viridis')
    plt.savefig("%s/scatter.png" %opts.wd)

    
#################################################
if __name__ == "__main__":
    main()
