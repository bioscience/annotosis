library(tidyverse)
library(RColorBrewer)
library(devtools)

args = commandArgs(trailingOnly=TRUE)
data <- read.table(args[1], header=TRUE, row.names=1)
data[, "EVIDENCE"] <- 100.0 - data[,"EVIDENCE"]
#summary(data)
data <- scale(data)
#data[, "EXONCNT"] <- -data[,"EXONCNT"]

cls <- kmeans(data, args[2])
write.table(cls$cluster, paste(args[3],".cluster", sep=""))
write.table(cls$centers, paste(args[3],".centers", sep=""))
write.table(cls$size, paste(args[3],".size", sep=""))

