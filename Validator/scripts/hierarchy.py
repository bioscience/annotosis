import os, sys, optparse
#import concurrent.futures
#from itertools import repeat
#from multiprocessing import Process, Manager, Queue, Lock
#import rpy2.robjects as robjects
#from rpy2.robjects import pandas2ri
import numpy as np
#import statistics as stats
from scipy.sparse import csr_matrix
#from sklearn.cluster import AgglomerativeClustering
from scipy.spatial.distance import pdist, squareform
from scipy.cluster.hierarchy import dendrogram
from scipy.cluster.hierarchy import fcluster
import matplotlib.pyplot as plt
import fastcluster
#from scipy.sparse import lil_matrix
#from scipy.io import mmwrite
sys.setrecursionlimit(1000000)

#################################################
def options():
    parser = optparse.OptionParser('usage: python %prog -i filename -n size')
    parser.add_option('-d', '--wd', dest='wd', help='Work directory', metavar='WORKDIR', default='')
    parser.add_option('-i', '--idxs', dest='idxs', help='Indices for gene identifiers in square matrix', metavar='IDXS', default='')
    parser.add_option('-m', '--matrix', dest='matrix', help='Bootstrapped matrix', metavar='MATRIX', default='')
    parser.add_option('-c', '--cnt', dest='cnt', help='Number of expected clusters', metavar='CNT', default='0')
    #parser.add_option('-T', '--threads', dest='threads', help='Number of parallel threads (default 24)', metavar='THREADS', default='24')
    #parser.add_option('-c', '--centers', dest='centers', help='Centers of bootstrapped kmeans clusters', metavar='CENTERS', default='')
    #parser.add_option('-s', '--sizes', dest='sizes', help='Sizes of bootstrapped kmeans clusters', metavar='SIZES', default='')
    #parser.add_option('-r', '--order', dest='order', action='store_true', help='Longest first (default=FALSE)', default=False)
    #parser.add_option('-a', '--assemble', dest='assemble', action='store_true', help='Do assembly', default=False)
    options, args = parser.parse_args()
    if options.matrix == '':
        parser.print_help()
        sys.exit(1)
    return options

#################################################
def seriation(Z, N, cur_index):
    '''
        input:
            - Z is a hierarchical tree (dendrogram)
            - N is the number of points given to the clustering process
            - cur_index is the position in the tree for the recursive traversal
        output:
            - order implied by the hierarchical tree Z
            
        seriation computes the order implied by a hierarchical tree (dendrogram)
    '''
    if cur_index < N:
        return [cur_index]
    else:
        left = int(Z[cur_index - N, 0])
        right = int(Z[cur_index - N, 1])
        return (seriation(Z, N, left) + seriation(Z, N, right))
    
#################################################
def compute_serial_matrix(dist_mat, method="ward"):
    '''
        input:
            - dist_mat is a distance matrix
            - method = ["ward","single","average","complete"]
        output:
            - seriated_dist is the input dist_mat,
              but with re-ordered rows and columns
              according to the seriation, i.e. the
              order implied by the hierarchical tree
            - res_order is the order implied by
              the hierarhical tree
            - res_linkage is the hierarhical tree (dendrogram)
        
        compute_serial_matrix transforms a distance matrix into 
        a sorted distance matrix according to the order implied 
        by the hierarchical tree (dendrogram)
    '''
    N = len(dist_mat[0])
    #flat_dist_mat = squareform(dist_mat)
    flat_dist_mat = dist_mat
    res_linkage = fastcluster.linkage(flat_dist_mat, method=method, preserve_input=True)
    res_order = seriation(res_linkage, N, N + N - 2)
    seriated_dist = np.zeros((N, N))
    a, b = np.triu_indices(N, k=1)
    seriated_dist[a,b] = dist_mat[[res_order[i] for i in a], [res_order[j] for j in b]]
    seriated_dist[b,a] = seriated_dist[a,b]
    
    return seriated_dist, res_order, res_linkage

#################################################
def main():
    '''
    '''
    opts = options()
    clusterCnt = int(opts.cnt)

    dGeneIdxs = {}
    with open(opts.idxs) as handle:
        for line in handle:
            items = line.strip().split('\t')
            gId, idx = items[0], items[1]
            dGeneIdxs[idx] = gId
    mat = np.load(opts.matrix)

    print ("### Clustering ...")
    gCnt = len(dGeneIdxs.keys())
    plt.pcolormesh(mat)
    plt.colorbar()
    plt.xlim([0, gCnt])
    plt.ylim([0, gCnt])
    #plt.show()
    plt.savefig("%s/clusters1.png" %opts.wd)
    matOrdered, res_order, Z = compute_serial_matrix(mat, method="ward")
    print ("### Clustering done ...")
    #matOrdered = fastcluster.linkage(mat, method ='ward')
    plt.pcolormesh(matOrdered)
    plt.xlim([0, gCnt])
    plt.ylim([0, gCnt])
    plt.savefig("%s/clusters2.png" %opts.wd)

    #print (res_order)
    with open("%s/geneOrdered.idxs" %opts.wd, 'w') as handle:
        for i in range(len(res_order)):
            handle.write("%s\t%d\n" %(dGeneIdxs["%d" %res_order[i]], i))
    np.save("%s/matOrdered.npy" %opts.wd, matOrdered)

    plt.figure(figsize =(12, 12))
    plt.title('Visualising the data')
    dendrogram(Z)
    plt.savefig('%s/dendro.png' %opts.wd)

    clusters = fcluster(Z, clusterCnt, criterion='maxclust')
    with open("%s/geneOrdered.idxs" %opts.wd, 'w') as handle:
        for i in range(len(res_order)):
            handle.write("%s\t%d\t%d\t%d\n" %(dGeneIdxs["%d" %res_order[i]], i, res_order[i], clusters[i]))
    np.save("%s/matOrdered.npy" %opts.wd, matOrdered)


    #cluster = AgglomerativeClustering(n_clusters = clusterCnt, affinity='euclidean', linkage='ward')
    #cluster.fit_predict(mat)
    #print (cluster.labels_)
    #print ("### Clustering done ...")
    ##with open("%s/clusters.txt" %opts.wd) as handle:
    ##    for i in range(len(cluster.labels_)):
    ##        handle.write("%s\t%d" %(gIds[i], cluster.labels_[i]))
    #
    ##ac2 = AgglomerativeClustering(n_clusters = 2)
    #
    ## Visualizing the clustering
    #plt.figure(figsize =(6, 6))
    #plt.scatter(X_principal['P1'], X_principal['P2'], c = ac2.fit_predict(X_principal), cmap ='rainbow')
    #
    ## plot the dendrogram
    #threshold = Z[-clusterCnt + 1, 2]
    #dg = shc.dendrogram(Z, no_labels = True, color_threshold = threshold)
    ## plot color bars under the leaves
    ##color = [classes[k] for k in dg['leaves']]
    ##b = .1 * Z[-1, 2]
    ##plt.bar(np.arange(N) * 10, np.ones(N) * b, bottom = -b, width = 10,
    ##        color = color, edgecolor = 'none')
    ##plt.gca().set_ylim((-b, None))
    #plt.savefig("clusters.png")
    #
    
#################################################
if __name__ == "__main__":
    main()
