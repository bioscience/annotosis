from snakemake.shell import shell

# Placeholder for optional parameters
extra = snakemake.params.get("extra", "")
# Run log
log = snakemake.log_fmt_shell()

index = snakemake.input.get("index")
reads = snakemake.input.get("reads")
#reads2 = snakemake.input.get("reads2")
#print (reads)

sThreads = int(snakemake.threads / 8)
if sThreads < 1: sThreads = 1
if sThreads > 12: sThreads = 12

if len(reads) == 2:
    input_flags = "-1 %s -2 %s" %(reads[0], reads[1])
#elif len(reads) > 2:
#    input_flags = "-1 %s -2 %s" %(','.join(reads1), ','.join(reads2))
else:
    raise RuntimeError(
        "Reads parameter must contain 2" " input files."
    )

# Executed shell command
shell(
    "(hisat2 {extra} "
    "--threads {snakemake.threads} "
    " -x {snakemake.input.index} {input_flags} "
    " | samtools view -@ {sThreads} -Sbh -u - "
    " | samtools sort -@ {sThreads} -o {snakemake.output[0]} -) "
    " {log}"
)

#def do_something(data_path, out_path, threads, myparam):
#        # python code#
#
#    do_something(snakemake.input[0], snakemake.output[0], snakemake.threads, snakemake.config["myparam"])
    
