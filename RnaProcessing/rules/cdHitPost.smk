rule cdHitEstPost:
    input:
        "{basename}.transdecoder.cds".format(basename=os.path.basename(config["cdhit"]["trs"]))
    output:
        "TrinityGG.cds.fasta"
    conda:
        "../envs/cdhit.yaml"
    log:
        "logs/cdhit/cdhitPost.log"
    threads: workflow.cores
    shell:
        "(cd-hit-est -i {input} -o {output} -c 0.99 -M 0 -T {threads}) > {log}"
