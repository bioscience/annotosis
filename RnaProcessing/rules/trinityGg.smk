#def getBam(wildcards):
#     return expand("mapped/{wildcards.genomeBase}.bam")
#     #return "trimmed/{sample}-{unit}.fastq.gz".format(**wildcards)

rule trinityGg:
    input:
        #getBam
        bam="mapped/{genomeBase}.bam".format(genomeBase=os.path.basename(config["reference"])),
    output:
        #"trinityGg"
        directory=directory("trinityGg"),
	#fasta="trinityGg/Trinity-GG.fasta"
	#fasta="trinityGg/Trinity-GG.fasta"
        #rename as ${output_dir}.Trinity.fasta
    params:
        maxIntron=config["maxIntronSize"]
    threads: workflow.cores
    log:
        "logs/trinity/trinityGg.log"
    conda:
        "../envs/trinity.yaml"
    resources:
        mem_gb=50
    shell:
        #" --full_cleanup --output {output.directory} "
        "(Trinity --CPU {threads} --max_memory {resources.mem_gb}G"
        " --output {output.directory} "
        " --genome_guided_bam {input.bam} "
        " --genome_guided_max_intron {params.maxIntron}) "
        " > {log}"
