#rule trinityDn:
#    input:
#        left=lambda wildcards: config["leftReadFiles"][wildcards.readFileId],
#	right=lambda wildcards: config["rightReadFiles"][wildcards.readFileId]
#    output:
#        directory("trinityDn")
#    log:
#        "logs/trinity/trinityDn.log"
#    conda:
#        "envs/trinity.yaml"
#    params:
#        extra="--normalize_reads"
#    threads: workflow.cores
#    resources:
#        mem_gb=50
#    script:
#         "scripts/trinityDn.py"
#
rule trinityDn:
    input:
        left=expand("trimmed/{reads}_1.fq.gz", reads = READS),
        right=expand("trimmed/{reads}_2.fq.gz", reads = READS)
    output:
        "trinityDn/Trinity.fasta"
    log:
        'logs/trinity/trinityDn.log'
    params:
        extra="--normalize_reads",
    threads: workflow.cores
    # optional specification of memory usage of the JVM that snakemake will respect with global
    # resource restrictions (https://snakemake.readthedocs.io/en/latest/snakefiles/rules.html#resources)
    # and which can be used to request RAM during cluster job submission as `{resources.mem_mb}`:
    # https://snakemake.readthedocs.io/en/latest/executing/cluster.html#job-properties
    resources:
        mem_gb=50
    wrapper:
        "0.68.0/bio/trinity"
