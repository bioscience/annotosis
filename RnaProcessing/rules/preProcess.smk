###########################################
# Preprocessing the read data
###########################################

rule trimmomatic_pe:
    input:
        r1="Reads/{reads}_1.fq.gz",
        r2="Reads/{reads}_2.fq.gz"
    output:
        r1="trimmed/{reads}_1.fq.gz",
	r2="trimmed/{reads}_2.fq.gz",
	# reads where trimming entirely removed the mate
	r1_unpaired="trimmed/{reads}_1.unpaired.fq.gz",
	r2_unpaired="trimmed/{reads}_2.unpaired.fq.gz"
    log:
        "logs/trimmomatic/{reads}.log"
    params:
        trimmer=["ILLUMINACLIP:{adapters}:2:30:10".format(adapters=ADAPTERS),"SLIDINGWINDOW:4:25","LEADING:25","TRAILING:25 MINLEN:60"],
        #trimmer=["ILLUMINACLIP:{adapters}:2:30:10 SLIDINGWINDOW:4:25 LEADING:25 TRAILING:25 MINLEN:60".format(adapters=ADAPTERS) for adapters in ADAPTERS],
        #extra="-phred33",
        compression_level="-9"
    threads:
        72
    # optional specification of memory usage of the JVM that snakemake will respect with global
    # resource restrictions (https://snakemake.readthedocs.io/en/latest/snakefiles/rules.html#resources)
    # and which can be used to request RAM during cluster job submission as `{resources.mem_mb}`:
    # https://snakemake.readthedocs.io/en/latest/executing/cluster.html#job-properties
    resources:
        mem_mb=4096
    wrapper:
        "0.68.0/bio/trimmomatic/pe"
