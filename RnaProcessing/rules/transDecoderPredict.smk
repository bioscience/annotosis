rule transdecoderPredict:
    input:
        fasta=config["cdhit"]["trs"],
        longorfs="{basename}.transdecoder_dir/longest_orfs.pep".format(basename=os.path.basename(config["cdhit"]["trs"]))
        #longorfs="transdecoder_dir/longest_orfs.pep"
    output:
        temp("{basename}.transdecoder.cds".format(basename=os.path.basename(config["cdhit"]["trs"]))),
        temp("{basename}.transdecoder.pep".format(basename=os.path.basename(config["cdhit"]["trs"]))),
        temp("{basename}.transdecoder.bed".format(basename=os.path.basename(config["cdhit"]["trs"]))),
        temp("{basename}.transdecoder.gff3".format(basename=os.path.basename(config["cdhit"]["trs"])))
    log:
        "logs/transdecoder/predict.log"
    params:
        extra=""
    wrapper:
        "0.68.0/bio/transdecoder/predict"

