rule hisat2_index:
    input:
        fasta = "{genome}",
    output:
        directory("index_{genome}")
    conda:
        "envs/hisat2.yaml"
    #log:
    #    "logs/hisat2_index.log"
    threads: workflow.cores
    shell:
        "mkdir -p {output}; hisat2-build -f -p {threads} {input.fasta} {output}"
    #wrapper:
    #    "0.68.0/bio/hisat2/index"

rule hisat2_align:
    input:
        index="index_{genome}",
        #reads1=expand("trimmed/{reads}_1.fq.gz", reads = READS),
	#reads2=expand("trimmed/{reads}_2.fq.gz", reads = READS)
        reads1="trimmed/{reads}_1.fq.gz",
	reads2="trimmed/{reads}_2.fq.gz"
    output:
        #"mapped/{genome}.bam"
        "mapped/{genome}_{reads}.bam"
    conda:
        "envs/hisat2.yaml"
    log:
        #"logs/hisat2_align_{genome}.log"
        "logs/hisat2_align_{genome}_{reads}.log"
    params:
        extra="--no-unal --dta --phred33 --fr --no-mixed --no-discordant",
    #    idx="index_{genome}/",
    threads: workflow.cores / 10
    script:
         "scripts/hisat2.py"
    #wrapper:
    #    "0.68.0/bio/hisat2/align"

rule samtools_merge:
    input:
        expand("mapped/{genome}_{reads}.bam", genome=GENOME, reads = READS)
    output:
        "mapped/{genome}.bam"
    params:
    "" # optional additional parameters as string
    threads: workflow.cores / 12 # This value - 1 will be sent to -@
    wrapper:
        "0.68.0/bio/samtools/merge"
