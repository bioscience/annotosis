#def getBams(wildcards):
#    return expand("mapped/{genomeBase}-{sample}-{unit}.bam", **wildcards)

rule samtoolsMerge:
    input:
        #getBams,
        expand("mapped/{genomeBase}-{sample}-{unit}.bam", genomeBase=os.path.basename(config["reference"]), sample=samples.loc[:,"sample"], unit=units.loc[:,"unit"])
        #expand("mapped/{genomeBase}-{sample}-{unit}.bam", genomeBase=os.path.basename(config["reference"]), sample=units.loc[:], unit=units.loc[:,"unit"])
        #expand("mapped/{genomeBase}-{unit}.bam", genomeBase=os.path.basename(config["reference"]), unit=units.loc[:,"unit"])
    output:
        "mapped/{genomeBase}.bam"
    log:
        "logs/samMerge/sam.merge.{genomeBase}.log"
    params:
        "" # optional additional parameters as string
    threads: workflow.cores
    wrapper:
        "0.68.0/bio/samtools/merge"
