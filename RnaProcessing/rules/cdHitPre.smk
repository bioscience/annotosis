rule cdHitEstPre:
    input:
        #"trinityGg/Trinity-GG.fasta"
        "trinityGg"
    output:
        config["cdhit"]["trs"]
    conda:
        "../envs/cdhit.yaml"
    log:
        "logs/cdhit/cdhitPre.log"
    threads: workflow.cores
    shell:
        "(cd-hit-est -i {input}/Trinity-GG.fasta -o {output} -c 0.99 -M 0 -T {threads}) > {log}"

