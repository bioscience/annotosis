def getFastqRna(wildcards):
    if config["trimming"]["skip"]:
        return units.loc[(wildcards.sample, wildcards.unit), ["fastq1", "fastq2"]].dropna()
    else:
        #if not is_single_end(**wildcards):
        #    # paired-end sample
        return expand("trimmed/{sample}-{unit}.{group}.fq.gz", group=[1, 2], **wildcards)
        # single end sample
        #return "trimmed/{sample}-{unit}.fastq.gz".format(**wildcards)
        #return ["trimmed/{sample}-{unit}.1.fastq.gz", "trimmed/{sample}-{unit}.2.fastq.gz"]

rule hisat2Align:
    input:
        index="index/{genomeBase}",
        #reads1=expand("trimmed/{reads}_1.fq.gz", reads = READS),
	#reads2=expand("trimmed/{reads}_2.fq.gz", reads = READS)
	reads=getFastqRna
        #reads1="trimmed/{reads}_1.fq.gz",
	#reads2="trimmed/{reads}_2.fq.gz"
    output:
        #"mapped/{genomeBase}.bam"
        "mapped/{genomeBase}-{sample}-{unit}.bam"
    conda:
        "../envs/hisat2.yaml"
    log:
        "logs/hisat2_align_{genomeBase}_{sample}_{unit}.log"
    params:
        extra="--no-unal --dta --phred33 --fr --no-mixed --no-discordant",
    threads: workflow.cores
    script:
         "../scripts/hisat2.py"
