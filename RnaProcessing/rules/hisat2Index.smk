rule hisat2Index:
    input:
        config["reference"]
    output:
        #directory("index_{genomeBase}".format(genomeBase=os.path.basename(config["reference"])))
        directory("index/{genomeBase}")
    conda:
        "../envs/hisat2.yaml"
    log:
        "logs/hisat2_index_{genomeBase}.log"
    threads: workflow.cores
    shell:
        "mkdir -p {output}; (hisat2-build -f -p {threads} {input} {output}) > {log}"
