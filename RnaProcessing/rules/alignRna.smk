#def getFastqRna(wildcards):
#    if config["trimming"]["skip"]:
#        return units.loc[(wildcards.sample, wildcards.unit), ["fastq1", "fastq2"]].dropna()
#    else:
#        #if not is_single_end(**wildcards):
#        #    # paired-end sample
#        return expand("trimmed/{sample}-{unit}.{group}.fastq.gz", group=[1, 2], **wildcards)
#        # single end sample
#        #return "trimmed/{sample}-{unit}.fastq.gz".format(**wildcards)
#        #return ["trimmed/{sample}-{unit}.1.fastq.gz", "trimmed/{sample}-{unit}.2.fastq.gz"]

rule hisat2Index:
    input:
        config["reference"]
    output:
        #directory("index_{genomeBase}".format(genomeBase=os.path.basename(config["reference"])))
        directory("index_{genomeBase}")
    conda:
        "../envs/hisat2.yaml"
    log:
        "logs/hisat2_index_{genomeBase}.log"
    threads: workflow.cores
    shell:
        "(hisat2-build -f -p {threads} {input} {output}) > {log}"
        #"mkdir -p {output}; (hisat2-build -f -p {threads} {fasta} {output}) > {log}"

#rule hisat2Align:
#    input:
#        index="index_{genomeBase}",
#        #reads1=expand("trimmed/{reads}_1.fq.gz", reads = READS),
#	#reads2=expand("trimmed/{reads}_2.fq.gz", reads = READS)
#	unit=getFastqRna
#        #reads1="trimmed/{reads}_1.fq.gz",
#	#reads2="trimmed/{reads}_2.fq.gz"
#    output:
#        #"mapped/{genome}.bam"
#        "mapped/{genomeBase}_{unit}.bam"
#    conda:
#        "envs/hisat2.yaml"
#    log:
#        "logs/hisat2_align_{genome}_{unit}.log"
#    params:
#        extra="--no-unal --dta --phred33 --fr --no-mixed --no-discordant",
#    threads: workflow.cores
#    script:
#         "scripts/hisat2.py"
#
#rule samtools_merge:
#    input:
#        expand("mapped/{genome}_{unit}.bam") #, genome=config["reference"], unit = )
#    output:
#        "mapped/{genome}.bam"
#    params:
#        "" # optional additional parameters as string
#    threads: workflow.cores
#    wrapper:
#        "0.68.0/bio/samtools/merge"

#rule align:
#    input:
#        sample=getFastqRna
#    output:
#        "star/{sample}-{unit}/Aligned.out.bam",
#        "star/{sample}-{unit}/ReadsPerGene.out.tab"
#    log:
#        "logs/star/{sample}-{unit}.log"
#    params:
#        # path to STAR reference genome index
#        index=config["ref"]["index"],
#        # optional parameters
#        extra="--quantMode GeneCounts --sjdbGTFfile {} {}".format(
#        config["ref"]["annotation"], config["params"]["star"])
#    threads: workflow.cores
#    wrapper:
#        "0.19.4/bio/star/align"

