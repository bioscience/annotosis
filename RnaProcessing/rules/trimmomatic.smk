def getFastq(wildcards):
    return units.loc[(wildcards.sample, wildcards.unit), ["fastq1", "fastq2"]].dropna()

rule trimmomaticPe:
    input:
        getFastq
    output:
        r1="trimmed/{sample}-{unit}.1.fq.gz",
	r2="trimmed/{sample}-{unit}.2.fq.gz",
	r1_unpaired=temp("trimmed/{sample}-{unit}.1.unpaired.fq.gz"),
	r2_unpaired=temp("trimmed/{sample}-{unit}.2.unpaired.fq.gz")
    log:
        "logs/trimmomatic/{sample}-{unit}.log"
    params:
        trimmer=["ILLUMINACLIP:{}:2:30:10".format(config["trimming"]["adapterFile"]),"SLIDINGWINDOW:4:25","LEADING:25","TRAILING:25","MINLEN:60"],
        extra="-phred33",
        compression_level="-9"
    conda:
        "../envs/trimmomatic.yaml"
    threads: workflow.cores
    resources:
        mem_mb=4096
    script:
         "../scripts/trimmomatic.py"
    #wrapper:
    #    "0.68.0/bio/trimmomatic/pe"

#rule cutadapt_pe:
#    input:
#        get_fastq
#    output:
#        fastq1="trimmed/{sample}-{unit}.1.fastq.gz",
#        fastq2="trimmed/{sample}-{unit}.2.fastq.gz",
#        qc="trimmed/{sample}-{unit}.qc.txt"
#    params:
#        "-a {} {}".format(config["trimming"]["adapter"], config["params"]["cutadapt-pe"])
#    log:
#        "logs/cutadapt/{sample}-{unit}.log"
#    wrapper:
#        "0.17.4/bio/cutadapt/pe"
