rule transdecoderLongorfs:
    input:
        fasta=config["cdhit"]["trs"]
    output:
        "{basename}.transdecoder_dir/longest_orfs.pep".format(basename=os.path.basename(config["cdhit"]["trs"]))
    log:
        "logs/transdecoder/longorfs.log"
    params:
        extra=""
    wrapper:
        "0.68.0/bio/transdecoder/longorfs"

